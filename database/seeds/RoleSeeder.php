<?php

use App\User;
use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * @var Role
     */
    private $role;
    /**
     * @var Permission
     */
    private $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function run()
    {
        $permissions = [
            ['name' => 'Создать пользователя'],
            ['name' => 'Редактировать пользователя'],
            ['name' => 'Удалить пользователя'],
        ];

        $permissionIds = [];
        foreach ($permissions as $permission) {
            $permissionIds[] = $this->permission->insertGetId($permission);
        }

        $roles = [
            ['name' => 'Админ', 'description' => 'Является собственником и имеет все права'],
            ['name' => 'Автор', 'description' => 'Является автором'],
        ];

        foreach ($roles as $value) {
            $role = $this->role->create($value);
            $role->permissions()->sync($permissionIds);
        }

        User::create([
            'name' => 'Admin',
            'login' => 'admin1',
            'email' => 'admin@gmail.com',
            'password' => 123123123,
            'active' => 1,
            'role_id' => User::ROLE_ADMIN_ID,
        ]);

        $user = User::create([
            'name' => 'Author',
            'login' => 'author1',
            'email' => 'author@gmail.com',
            'password' => 123123123,
            'active' => 1,
            'role_id' => User::ROLE_AUTHOR_ID,
        ]);
        \App\Author::create([
            'description' => 'Author description',
            'address' => 'Bishkek',
            'phone' => 1,
            'user_id' => $user->id,
        ]);
    }
}
