<?php

use App\Event;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++){
            Event::query()->create([
                'name' => "Событие $i",
                'price' => 100 + $i,
                'description' => "Описание собитии $i",
                'period' => 5 + $i,
                'start_date_time' => new DateTime(),
                'start_place' => 'ЦУМ',
                'user_id' => \App\User::query()->where('role_id', \App\User::ROLE_AUTHOR_ID)->first()->id,
            ]);
        }
    }
}
