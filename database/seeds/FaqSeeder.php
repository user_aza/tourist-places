<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++){
            Faq::query()->create([
                'question' => "Вопрос $i",
                'answer' => "Ответ $i",
            ]);
        }
    }
}
