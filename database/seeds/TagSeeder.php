<?php

use App\Faq;
use App\Subscribe;
use App\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        for ($i = 1; $i < 6; $i++){
            $tag = Tag::query()->create([
                'name' => "Тег $i",
                'description' => "Описание тега $i",
            ]);
            DB::table('taggables')->insert([
                'tag_id' => $tag->id,
                'taggable_id' => $i,
                'taggable_type' => ($i % 2 == 0) ? \App\Event::class : \App\Place::class,
            ]);
        }
    }
}
