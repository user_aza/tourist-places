<?php

use App\Place;
use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++){
            Place::query()->create([
                'name' => "Место $i",
                'description' => "Описание места $i",
                'lat' => "42.87",
                'lng' => "74.59",
            ]);
        }
    }
}
