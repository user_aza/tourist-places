const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss')

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

// mix.copy('./fonts', '../../../public/modules/saving/fonts');
// mix.copy('./images', '../../../public/modules/saving/images');
mix.options({
    processCssUrls: false,
    postCss: [
        require('autoprefixer')(),
        tailwindcss('./tailwind.config.js'),
    ]
});
