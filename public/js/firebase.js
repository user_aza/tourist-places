const firebaseConfig = {
    apiKey: "AIzaSyBSOd_66yAHStPXcAP7KTjyN_ia401S6ZM",
    authDomain: "send-message-457bb.firebaseapp.com",
    databaseURL: "https://send-message-457bb.firebaseio.com",
    projectId: "send-message-457bb",
    storageBucket: "send-message-457bb.appspot.com",
    messagingSenderId: "966029939707",
    appId: "1:966029939707:web:d4530fbdca0b2a51e7315e",
    measurementId: "G-13JMQLTG48"
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.usePublicVapidKey("BEARuaXmDcCInTp1VHd3GbiiOJiU939lj4JjUUPff77vRqD3mPkSU3Jz9lWVzdUIE-zSo78_dsSy4zDHBpXnedM");
messaging
    .requestPermission()
    .then(function () {
        console.log("Notification permission granted.");
        return messaging.getToken()
    }).then(function (token) {
        console.log(token)
    })
    .catch(function (err) {
        console.log("Unable to get permission to notify,", err);
    });

messaging.requestPermission()
    .then(function() {
        console.log('Notification permission granted.');
    })
    .catch(function(err) {
        console.log('Unable to get permission to notify. ', err);
    });

messaging.onTokenRefresh(() => {
    retrieveToken()
});

messaging.onMessage((payload) => {
    console.log('Message received.');
    console.log(payload);
});

retrieveToken();




function sendTokenToServer(fcm_token) {
    fetch('/admin/users/save-token/' + fcm_token)
        .then(response => response.json())
        .then(res => console.log(res))
}

function retrieveToken() {
    messaging.getToken().then((fcm_token) => {
        if (fcm_token) {
            console.log('Token received :' + fcm_token);
            sendTokenToServer(fcm_token);
        } else {
            alert('You should allow notification!')
        }
    }).catch((err) => {
        console.log(err.message)
    });
}
