importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.5/firebase-messaging.js');

var firebaseConfig = {
    apiKey: "AIzaSyDsGqQwpXap0W7-cPok7hIsMuZvvw4RmFo",
    authDomain: "tourist-195af.firebaseapp.com",
    databaseURL: "https://tourist-195af.firebaseio.com",
    projectId: "tourist-195af",
    storageBucket: "tourist-195af.appspot.com",
    messagingSenderId: "496488124087",
    appId: "1:496488124087:web:76d2bc6856276e00f4c732"
};
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();





messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    const {title, body} = payload.notification;
    const notificationOptions = {body};

    return self.registration.showNotification(title, notificationOptions);
});
