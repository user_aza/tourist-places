module.exports = {
    theme: {
        extend: {},
        pagination: theme => ({
            color: theme('colors.gray.600'),
        })

    },
    variants: {},
    plugins: [
        require('tailwindcss-plugins/pagination'),
    ],
    corePlugins: {
        outline: false,
    }
}
