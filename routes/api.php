<?php

use App\Http\Controllers\Api\AuthorController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\FaqController;
use App\Http\Controllers\Api\PlaceController;
use App\Http\Controllers\Api\TagController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('check-login', 'Auth\RegisterController@checkLogin');
Route::post('check-email', 'Auth\RegisterController@checkEmail');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'api', 'prefix' => 'password'], function () {
    Route::post('email', 'Auth\PasswordResetController@create');
    Route::post('reset', 'Auth\PasswordResetController@reset');
});

Route::prefix("/")->middleware(['auth:api'])->group(function () {
    Route::get('me', [UserController::class, 'me']);
    Route::post('save-fcm-token', [UserController::class, 'saveFcmToken']);
    Route::put('profile-update',  [UserController::class, 'profileUpdate']);

    Route::group(['prefix' => 'faq'], function (){
        Route::get('/', [FaqController::class, 'faq']);
    });

    Route::group(['prefix' => 'events'], function (){
        Route::get('/', [EventController::class, 'index']);
        Route::get('/favorites', [EventController::class, 'favorites']);
        Route::get('/{id}', [EventController::class, 'show']);
        Route::post('/{id}/favorite/{like}', [EventController::class, 'favorite']);
    });

    Route::group(['prefix' => 'places'], function () {
        Route::get('/', [PlaceController::class, 'index']);
        Route::get('/favorites', [PlaceController::class, 'favorites']);
        Route::get('/{id}', [PlaceController::class, 'show']);
        Route::post('/{id}/favorite/{like}', [PlaceController::class, 'favorite']);
        Route::post('/{id}/subscribe', [PlaceController::class, 'subscribe']);
    });

    Route::group(['prefix' => 'authors'], function () {
        Route::get('/{id}', [AuthorController::class, 'show']);
        Route::post('/{id}/subscribe',  [AuthorController::class, 'subscribe']);
    });
    Route::group(['prefix' => 'tags'], function () {
        Route::get('/', [TagController::class, 'index']);
        Route::get('/{id}/places', [TagController::class, 'places']);
        Route::get('/{id}/events', [TagController::class, 'events']);
        Route::post('/{id}/subscribe',  [TagController::class, 'subscribe']);
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::post('/allow-notifications', [UserController::class, 'allowNotifications']);
    });
});

