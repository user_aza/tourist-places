<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\EventController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\PlaceController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::view('app-privacy-policy', 'privacy_policy');

Route::get('test-fcm-message', [UserController::class, 'testFcmMessage']);


Route::get('admin/login', [AuthController::class, 'form'])->name('admin.auth.form');
Route::post('admin/login', [AuthController::class, 'login'])->name('admin.auth.login');
Route::get('admin/logout', [AuthController::class, 'logout'])->name('admin.auth.logout');

Route::prefix("admin")->middleware(['auth:web'])->group(function () {
    Route::view('/', 'admin.layout.dashboard')->name('admin.dashboard');
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index'])->name('admin.user');
        Route::get('/create', [UserController::class, 'create'])->name('admin.user.create');
        Route::post('/store', [UserController::class, 'store'])->name('admin.user.store');
        Route::get('/{id}/edit', [UserController::class, 'edit'])->name('admin.user.edit');
        Route::post('/{id}/update', [UserController::class, 'update'])->name('admin.user.update');
        Route::get('{id}/delete', [UserController::class, 'delete'])->name('admin.user.delete');
    });

    Route::group(['prefix' => 'role'], function () {
        Route::get('/', [RoleController::class, 'index'])->name('admin.role');
        Route::get('/create', [RoleController::class, 'create'])->name('admin.role.create');
        Route::post('/store', [RoleController::class, 'store'])->name('admin.role.store');
        Route::get('/edit/{id}', [RoleController::class, 'edit'])->name('admin.role.edit');
        Route::post('/update/{id}', [RoleController::class, 'update'])->name('admin.role.update');
        Route::get('/delete/{id}', [RoleController::class, 'delete'])->name('admin.role.delete');
    });

    Route::group(['prefix' => 'permission'], function () {
        Route::get('/', [PermissionController::class, 'index'])->name('admin.permission');
        Route::get('/create', [PermissionController::class, 'create'])->name('admin.permission.create');
        Route::post('/store', [PermissionController::class, 'store'])->name('admin.permission.store');
        Route::get('/edit/{id}', [PermissionController::class, 'edit'])->name('admin.permission.edit');
        Route::post('/update/{id}', [PermissionController::class, 'update'])->name('admin.permission.update');
        Route::get('/delete/{id}', [PermissionController::class, 'delete'])->name('admin.permission.delete');
    });

    Route::group(['prefix' => 'event'], function () {
        Route::get('/', [EventController::class, 'index'])->name('admin.event');
        Route::get('/create', [EventController::class, 'create'])->name('admin.event.create');
        Route::post('/store', [EventController::class, 'store'])->name('admin.event.store');
        Route::get('/edit/{id}', [EventController::class, 'edit'])->name('admin.event.edit');
        Route::post('/update/{id}', [EventController::class, 'update'])->name('admin.event.update');
        Route::get('/delete/{id}', [EventController::class, 'delete'])->name('admin.event.delete');
    });

    Route::group(['prefix' => 'places'], function () {
        Route::get('/', [PlaceController::class, 'index'])->name('admin.place');
        Route::get('/create', [PlaceController::class, 'create'])->name('admin.place.create');
        Route::post('/store', [PlaceController::class, 'store'])->name('admin.place.store');
        Route::get('/{id}/edit', [PlaceController::class, 'edit'])->name('admin.place.edit');
        Route::post('/{id}/update', [PlaceController::class, 'update'])->name('admin.place.update');
        Route::get('/{id}/delete', [PlaceController::class, 'delete'])->name('admin.place.delete');
    });

    Route::group(['prefix' => 'media'], function () {
        Route::get('/', [MediaController::class, 'index'])->name('admin.media.index');
        Route::post('/create', [MediaController::class, 'store'])->name('admin.media.store');
        Route::get('/delete/{id}', [MediaController::class, 'delete'])->name('admin.media.delete');
    });

    Route::group(['prefix' => 'tag'], function () {
        Route::get('/', [TagController::class, 'index'])->name('admin.tag');
        Route::get('/create', [TagController::class, 'create'])->name('admin.tag.create');
        Route::post('/store', [TagController::class, 'store'])->name('admin.tag.store');
        Route::get('/edit/{id}', [TagController::class, 'edit'])->name('admin.tag.edit');
        Route::post('/update/{id}', [TagController::class, 'update'])->name('admin.tag.update');
        Route::get('/delete/{id}', [TagController::class, 'delete'])->name('admin.tag.delete');
    });

    Route::group(['prefix' => 'faq'], function () {
        Route::get('/', [FaqController::class, 'index'])->name('admin.faq');
        Route::get('/create', [FaqController::class, 'create'])->name('admin.faq.create');
        Route::post('/store', [FaqController::class, 'store'])->name('admin.faq.store');
        Route::get('/edit/{id}', [FaqController::class, 'edit'])->name('admin.faq.edit');
        Route::post('/update/{id}', [FaqController::class, 'update'])->name('admin.faq.update');
        Route::get('/delete/{id}', [FaqController::class, 'delete'])->name('admin.faq.delete');
    });
});



