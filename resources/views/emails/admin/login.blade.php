@component('mail::message')
# Уведомление!

Администратор <b>{{$user->name}}</b>
<br>
вошел в админ панель!
{{$user->email}}
@component('mail::button', ['url' => config('app.url')])
На сайт
@endcomponent

Спасибо,<br>
{{ config('app.name') }}
@endcomponent
