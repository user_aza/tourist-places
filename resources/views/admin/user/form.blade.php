@extends('admin.layout.master')
@section('content')
    <div>
        <div class="flex justify-between mb-5">
            <p class="text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ url()->previous() }}">
                <i class="fas fa-step-backward"></i>
            </a>
        </div>

        <form action="{{$action}}" method="post" enctype="multipart/form-data">
            <div>
                @csrf
                @include('admin.elements.form.input', ['label' => 'Имя', 'attribute'=>'name', 'placeholder'=> 'Имя', 'value' => !!old('name') ? old('name') : (!!isset($user) ? $user->name : ''), 'error'=>$errors->first('name')])
                @include('admin.elements.form.input', ['label' => 'Логин', 'attribute'=>'login', 'placeholder'=> 'Login', 'value' => !!old('login') ? old('login') : (!!isset($user) ? $user->login : ''), 'error'=>$errors->first('login')])
                @include('admin.elements.form.input', ['label' => 'Email', 'attribute'=>'email', 'placeholder'=> 'Email', 'value' => !!old('email') ? old('email') : (!!isset($user) ? $user->email : ''), 'error'=>$errors->first('email')])
                @include('admin.elements.form.input', ['label' => 'Пароль', 'attribute'=>'password', 'placeholder'=> '********', 'value' => '', 'error'=>$errors->first('password'), 'type' => 'password'])
                @include('admin.elements.form.input', ['label' => 'Адрес', 'attribute'=>'address', 'placeholder'=> 'address', 'value' => !!old('address') ? old('address') : ((!!isset($user) && $user->author) ? $user->author->address : ''), 'error'=>$errors->first('address')])
                @include('admin.elements.form.input', ['label' => 'Телефон', 'attribute'=>'phone', 'placeholder'=> 'phone', 'value' => !!old('phone') ? old('phone') : ((!!isset($user) && $user->author) ? $user->author->phone : ''), 'error'=>$errors->first('phone')])
                @include('admin.elements.form.editor', ['label' => 'Описание', 'attribute'=>'description', 'placeholder'=> 'description', 'value' => !!old('description') ? old('description') : ((!!isset($user) && $user->author) ? $user->author->description : ''), 'error'=>$errors->first('description')])
                @include('admin.elements.form.select', ['label' => 'Роль', 'attribute'=> 'role_id', 'options' => $roles, 'value'=> !!old('role_id') ? old('role_id') : (isset($user) ? $user->role_id : ''), 'error' => $errors->first('role_id')])
                @include('admin.elements.form.checkbox', ['label' => 'Активен', 'attribute'=> 'active', 'value'=> !!isset($user) ? $user->active : 1])
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Аватар</label>
                    <input type="file" name="src"
                           class="form-control shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight border-gray-500 bg-white">
                    @if(isset($user))
                        @foreach($user->medias as $media)
                            @if($media->main)
                                <div class="col-md-8 m-2">
                                    <img width="80" src="{{$media->src}}"/>
                                    <a href="{{route('admin.media.delete', ['id' => $media->id])}}" class="">Удалить</a>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>

                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Фон</label>
                    <input type="file" name="bg"
                           class="form-control shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight border-gray-500 bg-white">
                    @if(isset($user))
                        @foreach($user->medias as $media)
                            @if(!$media->main)
                                <div class="col-md-8 m-2">
                                    <img width="80" src="{{$media->src}}"/>
                                    <a href="{{route('admin.media.delete', ['id' => $media->id])}}" class="">Удалить</a>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
                <div class="p-3">
                    <input type="submit"
                           class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                           value="{{$submitName}}">
                </div>
            </div>
        </form>
    </div>
@endsection

