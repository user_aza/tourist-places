@extends('admin.layout.master')
@section('content')
    <div class="container">
        <div class="flex justify-between mb-5 bg">
            <p class="ml-3 px-3 text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
                href="{{ route('admin.user.create') }}">
                <i class="far fa-plus-square"></i>
            </a>
        </div>
        <div class="mb-5">
            {!! $filterForm !!}
        </div>
        <table class="w-full text-left table-collapse">
            <thead class="thead">
            <tr>
                <th class="p-2">Id</th>
                <th class="p-2">Имя</th>
                <th class="p-2">Login</th>
                <th class="p-2">Email</th>
                <th class="p-2">Роль</th>
                <th class="p-2">Актив</th>
                <th class="p-2">Аватар</th>
                <th class="p-2">Фон</th>
                <th class="p-2 text-right pr-12">Действия</th>
            </tr>
            </thead>
            <tbody class="align-baseline">
            @foreach($users as $user)
                <tr>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$user->id}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$user->name}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$user->login}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$user->email}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        @if($user->role){{$user->role->name}} @else -- @endif
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        @if($user->active) да @else нет @endif
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        @foreach($user->medias as $media)
                            @if($media->main)
                                <img width="50" src="{{$media->src}}" alt="">
                            @endif
                        @endforeach
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        @foreach($user->medias as $media)
                            @if(!$media->main)
                                <img width="50" src="{{$media->src}}" alt="">
                            @endif
                        @endforeach
                    </td>
                    <td class="p-3 border-t border-gray-300 font-mono text-xs font-bold text-gray-700 mr-16 text-right">
                        <div class="inline-flex">
                            <a href="{{ route('admin.user.edit', ['id' => $user->id]) }}"
                               class="pr-10">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="{{ route('admin.user.delete', ['id' => $user->id]) }}"
                               class="pr-10">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->appends(request()->all())->links() }}
    </div>
@endsection
