<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
<div class="flex justify-center items-center h-screen " style="background: #585858;">
        <form action="{{route('admin.auth.login')}}"
              method="post" class="shadow-md rounded"
              style="background: #dadada; width: 400px; padding: 33px"
        >
            @csrf
            <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">Email</label>
                <label>
                    <input type="text" name="email" placeholder="Email" value="{{old('email')}}"
                           class="shadow-lg appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3
                               @if($errors->first('email')) border-red-500 @else border-gray-500 @endif">
                </label>
                <p class="text-red-500 text-xs italic">{{$errors->first('email')}}</p>
            </div>
            <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2">Пароль</label>
                <label>
                    <input name="password" type="password" placeholder="******"
                           class="shadow-lg appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3
                                @if($errors->first('password')) border-red-500 @else border-gray-500 @endif">
                </label>
                <p class="text-red-500 text-xs italic">{{$errors->first('password')}}</p>
                <p class="text-red-500 text-xs italic">{{$errors->first('auth_fail')}}</p>
            </div>
            <div class="flex items-center justify-between">
                <input
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                type="submit" value="Войти">
            </div>
        </form>
</div>

</body>
</html>
