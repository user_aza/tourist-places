@extends('admin.layout.master')
@section('content')
    <div>
        <div class="flex justify-between mb-5">
            <p class="text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ url()->previous() }}">
                <i class="fas fa-step-backward"></i>
            </a>
        </div>

        <form action="{{$action}}" method="post">
            @csrf
            <div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Имя</label>
                    <input type="text" name="name" placeholder="Username"
                           @if(isset($role)) value="{{$role->name}}" @endif
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                           @if($errors->first('name')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('name')}} </p>
                    <label class="text-gray-700 text-ms font-bold mb-2">Описание</label>
                    <textarea
                        type="text" name="description" placeholder="Description"
                        class="editor shadow appearance-none border border-gray-500 rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight">@if(isset($role)){{$role->description}}@endif</textarea>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Права</label>
                    @foreach($permissions as $permission)
                        <div>
                            <input id="permission_{{$permission->id}}" type="checkbox" name="permissions[]" value="{{$permission->id}}"
                                   @if(isset($role)) @if($role->permissions->contains($permission->id)) checked @endif @endif>
                            <label for="permission_{{$permission->id}}">{{$permission->name}}</label>
                        </div>
                    @endforeach
                </div>
                <div class="p-3">
                    <input type="submit"
                           class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                           value="{{$submitName}}">
                </div>
            </div>
        </form>
    </div>
@endsection
