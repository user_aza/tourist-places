@extends('admin.layout.master')
@section('content')
    <div>
        <div class="flex justify-between mb-5">
            <p class="text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ url()->previous() }}">
                <i class="fas fa-step-backward"></i>
            </a>
        </div>

        <form action="{{$action}}" method="post" enctype="multipart/form-data">
            @csrf
            <div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Имя</label>
                    <input type="text" name="name" placeholder="Username"
                           value="@if(old('name')){{old('name')}}@elseif(isset($tag)){{$tag->name}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                           @if($errors->first('name')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('name')}} </p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Описание</label>
                    <textarea name="description" placeholder="Описание"
                              class="editor shadow appearance-none border border-gray-500 rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight"
                    >@if(isset($tag)){{$tag->description}}@endif</textarea>
                </div>
{{--                <div class="px-3 my-5">--}}
{{--                    <label class="text-gray-700 text-ms font-bold mb-2">Места</label>--}}
{{--                    <div class="flex flex-wrap items-center bg-gray-100 rounded-lg py-5 border border-gray-500">--}}
{{--                        @foreach($places as $place)--}}
{{--                            <div class="px-4 rounded">--}}
{{--                                <label for="{{"places[$place->id]"}}">--}}
{{--                                    <h1 name="{{"places[$place->id]"}}" id="{{"places[$place->id]"}}"></h1>--}}
{{--                                    {{ $place->name }}--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="px-3 my-5">--}}
{{--                    <label class="text-gray-700 text-ms font-bold mb-2">События</label>--}}
{{--                    <div class="flex flex-wrap items-center bg-gray-100 rounded-lg py-5 border border-gray-500">--}}
{{--                        @foreach($events as $event)--}}
{{--                            <div class="px-4 rounded">--}}
{{--                                <label for="{{"events[$event->id]"}}">--}}
{{--                                    <h1 name="{{"events[$event->id]"}}" id="{{"events[$event->id]"}}"></h1>--}}
{{--                                    {{ $event->name }}--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="p-3">
                    <input type="submit"
                           class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                           value="{{$submitName}}">
                </div>
            </div>
        </form>
    </div>
@endsection

