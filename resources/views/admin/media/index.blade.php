@extends('admin.layout.master')
@section('content')
    <div class="container bg-white my-5 p-5 shadow-lg rounded-lg">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong><br>
                {{--                <ul>--}}
                {{--                    @foreach ($errors->all() as $error)--}}
                {{--                        <li>{{ $error }}</li>--}}
                {{--                    @endforeach--}}
                {{--                </ul>--}}
            </div>
        @endif
        <form method="post" action="{{route('admin.media.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="p-3">
                <input type="file" name="src[]" multiple class="form-control">
                <input type="submit"
                       class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                       value="Save">
            </div>
            <hr>
            <div>
                @include('admin.elements.list.images', ['images' => $medias])
            </div>
        </form>
    </div>
@endsection
