@extends('admin.layout.master')
@section('content')
    <div class="container">
        <div class="flex justify-between mb-5 bg">
            <p class="ml-3 px-3 text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ route('admin.event.create') }}">
                <i class="far fa-plus-square"></i>
            </a>
        </div>
        <div class="mb-5">
            {!! $filterForm !!}
        </div>
        <table class="w-full text-left table-collapse">
            <thead class="thead">
            <tr>
                <th class="p-2">Id</th>
                <th class="p-2">Имя</th>
                <th class="p-2">Цена</th>
                <th class="p-2">Дни</th>
                <th class="p-2">Дата</th>
                <th class="p-2">Место</th>
                <th class="p-2">Автор</th>
                <th class="p-2">Места</th>
                <th class="p-2">Тег</th>
                <th class="p-2">Актив</th>
                <th class="p-2 text-right pr-12">Действия</th>
            </tr>
            </thead>
            <tbody class="align-baseline">
            @foreach($events as $event)
                <tr>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->id}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->name}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->price}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->period}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->start_date_time}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->start_place}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        @if($event->user){{$event->user->name}}@endif
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->places->count()}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$event->tags->count()}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        @if($event->active) да @else нет @endif
                    </td>
                    <td class="p-3 border-t border-gray-300 font-mono text-xs font-bold text-gray-700 mr-16 text-right">
                        <div class="inline-flex">
                            <a href="{{ route('admin.event.edit', ['id' => $event->id]) }}"
                               class="pr-10">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="{{ route('admin.event.delete', ['id' => $event->id]) }}"
                               class="pr-10">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $events->appends(request()->all())->links() }}
    </div>
@endsection
