@extends('admin.layout.master')
@section('content')
    <div>
        <div class="flex justify-between mb-5">
            <p class="text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ url()->previous() }}">
                <i class="fas fa-step-backward"></i>
            </a>
        </div>

        <form action="{{$action}}" method="post" enctype="multipart/form-data">
            @csrf
            <div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Заголовок</label>
                    <input type="text" name="name" placeholder="Заголовок"
                           value="@if(old('name')){{old('name')}}@elseif(isset($event)){{$event->name}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                           @if($errors->first('name')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('name')}} </p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Цена</label>
                    <input type="text" name="price" placeholder="Цена"
                           value="@if(old('price')){{old('price')}}@elseif(isset($event)){{$event->price}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight
                           @if($errors->first('price')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('price')}}</p>
                </div>
                @include('admin.elements.form.textarea', ['label' => 'Описание', 'attribute'=>'description', 'placeholder'=> 'Описание', 'value' => !!old('description') ? old('description') : (isset($event) ? $event->description : ''), 'error'=>$errors->first('description'), 'class' => 'editor'])
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Дни</label>
                    <input name="period" type="text" placeholder="Дни"
                           value="@if(old('period')){{old('period')}}@elseif(isset($event)){{$event->period}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                            @if($errors->first('period')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('period')}}</p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Дата</label>
                    <input name="start_date_time" type="datetime-local"
                           value="@if(old('start_date_time')){{old('start_date_time')}}@elseif(isset($event)){{$event->start_date_time}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                            @if($errors->first('start_date_time')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('start_date_time')}}</p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Пункт назначения</label>
                    <input name="start_place" type="text" placeholder="Пункт назначения"
                           value="@if(old('start_place')){{old('start_place')}}@elseif(isset($event)){{$event->start_place}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                            @if($errors->first('start_place')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('start_place')}}</p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Автор</label>
                    <select name="user_id"
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight border-gray-500">
                        @foreach($users as $user)
                            <option value="{{$user->id}}"
                                    @isset($event) @if($event->user_id == $user->id)  selected @endif @endisset>{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Места</label>
                    <select name="placeIds[]" multiple
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight border-gray-500">
                        @foreach($places as $place)
                            <option value="{{$place->id}}"
                                    @isset($event) @if($event->places && $event->places->contains($place->id))  selected @endif @endisset>{{$place->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Тег</label>
                    <select name="tagIds[]" multiple
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight border-gray-500 bg-white">
                        <option value="">Выберите значение</option>
                        @foreach($tags as $tag)
                            <option value="{{$tag->id}}"
                                    @isset($event) @if($event->tags->contains($tag->id))  selected @endif @endisset>{{$tag->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="checkbox px-5">
                    <label class="text-gray-700 text-ms font-bold mb-2" for="active">
                        <input type="checkbox" name="active" id="active"
                               @isset($event) @if($event->active) checked @endif @endisset>
                        Активен
                    </label>
                </div>
                <div class="p-3"><input type="file" name="src[]" multiple class="form-control"></div>

                <div class="p-3">
                    <input type="submit"
                           class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                           value="{{$submitName}}">
                </div>
            </div>
        </form>
        @if(isset($event))
            @include('admin.elements.list.images', ['images' => $event->medias])
        @endif
    </div>
@endsection

