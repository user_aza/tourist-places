@extends('admin.layout.master')
@section('content')
    <div>
        <div class="flex justify-between mb-5">
            <p class="ml-3 px-3 text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
                href="{{ route('admin.permission.create') }}">
                <i class="far fa-plus-square"></i>
            </a>
        </div>
        <div class="mb-5">
            {!! $filterForm !!}
        </div>
        <table class="w-full text-left table-collapse">
            <thead class="thead">
            <tr>
                <th class="p-2">Id</th>
                <th class="p-2">Имя</th>
                <th class="p-2">Роль</th>
                <th class="p-2 text-right pr-12">Действия</th>
            </tr>
            </thead>
            <tbody class="align-baseline">
            @foreach($permissions as $permission)
                <tr>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$permission->id}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$permission->name}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$permission->roles->count()}}
                    </td>
                    <td class="p-3 border-t border-gray-300 font-mono text-xs font-bold text-gray-700 mr-16 text-right">
                        <div class="inline-flex">
                            <a href="{{ route('admin.permission.edit', ['id' => $permission->id]) }}"
                               class="pr-10">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="{{ route('admin.permission.delete', ['id' => $permission->id]) }}"
                               class="pr-10">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $permissions->appends(request()->all())->links() }}

    </div>
@endsection


