@extends('admin.layout.master')
@section('content')
    <div>
        <div class="flex justify-between mb-5">
            <p class="text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ url()->previous() }}">
                <i class="fas fa-step-backward"></i>
            </a>
        </div>

        <form action="{{$action}}" method="post" enctype="multipart/form-data">
            @csrf
            <div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Имя</label>
                    <input type="text" name="name" placeholder="Username"
                           value="@if(old('name')){{old('name')}}@elseif(isset($place)){{$place->name}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                           @if($errors->first('name')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('name')}} </p>
                </div>
                @include('admin.elements.form.textarea', ['label' => 'Описание', 'attribute'=>'description', 'placeholder'=> 'Описание', 'value' => !!old('description') ? old('description') : (isset($place) ? $place->description : ''), 'error'=>$errors->first('description'), 'class' => 'editor'])

                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Широта</label>
                    <input type="text" name=
                    "lat" placeholder="Широта"
                           value="@if(old('lat')){{old('lat')}}@elseif(isset($place)){{$place->lat}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                           @if($errors->first('lat')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('lat')}} </p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Долгота</label>
                    <input type="text" name="lng" placeholder="Долгота"
                           value="@if(old('lng')){{old('lng')}}@elseif(isset($place)){{$place->lng}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                           @if($errors->first('lng')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('lng')}} </p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Тег</label>
                    <select name="tagIds[]" multiple
                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight border-gray-500 bg-white">
                        <option value="">Выберите значение</option>
                        @foreach($tags as $tag)
                            <option value="{{$tag->id}}"
                                    @isset($place) @if($place->tags->contains($tag->id))  selected @endif @endisset>{{$tag->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Медиа</label>

                    <input type="file" name="src[]" multiple
                           class="form-control shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight border-gray-500 bg-white">
                    @if(isset($place))
                        @include('admin.elements.list.images', ['images' => $place->medias])
                    @endif
                </div>
                <div class="p-3">
                    <input type="submit"
                           class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                           value="{{$submitName}}">
                </div>
            </div>
        </form>
    </div>
@endsection

