@extends('admin.layout.master')
@section('content')
    <div class="container">
        <div class="flex justify-between mb-5 bg">
            <p class="ml-3 px-3 text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ route('admin.place.create') }}">
                <i class="far fa-plus-square"></i>
            </a>
        </div>
        <div class="mb-5">
            {!! $filterForm !!}
        </div>
        <table class="w-full text-left table-collapse">
            <thead class="thead">
            <tr>
                <th class="p-2">Id</th>
                <th class="p-2">Имя</th>
                <th class="p-2">Широта</th>
                <th class="p-2">Долгота</th>
{{--                <th class="p-2">Тег</th>--}}
{{--                <th class="p-2">Медиа</th>--}}
                <th class="p-2 text-right pr-12">Действия</th>
            </tr>
            </thead>
            <tbody class="align-baseline">
            @foreach($places as $place)
                <tr>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$place->id}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$place->name}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$place->lat}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$place->lng}}
                    </td>
{{--                    <td class="p-2 border-t border-gray-300 font-mono text-xs">--}}
{{--                        @if($place)--}}
{{--                            @foreach($place->tags as $tag)--}}
{{--                                {{$tag->name}}--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                    </td>--}}
{{--                    <td class="p-2 border-t border-gray-300 font-mono text-xs">--}}
{{--                        @if($place->medias->first())--}}
{{--                            <img width="50" src="{{$place->medias->first()->src}}" alt="">--}}
{{--                        @endif--}}
{{--                    </td>--}}
                    <td class="p-3 border-t border-gray-300 font-mono text-xs font-bold text-gray-700 mr-16 text-right">
                        <div class="inline-flex">
                            <a href="{{ route('admin.place.edit', ['id' => $place->id]) }}"
                               class="pr-10">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="{{ route('admin.place.delete', ['id' => $place->id]) }}"
                               class="pr-10">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $places->appends(request()->all())->links() }}
    </div>
@endsection
