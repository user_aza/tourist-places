<div class="checkbox px-5">
    <label class="text-gray-700 text-ms font-bold mb-2" for="{{$attribute}}">
    <input type="checkbox" name="{{$attribute}}" id="{{$attribute}}"
               @if($value) checked @endif>
        {{$label}}
    </label>
</div>
