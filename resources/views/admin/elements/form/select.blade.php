<div class="p-3">
    <label class="text-gray-700 text-ms font-bold mb-2">{{$label}}</label>
    <select name="{{$attribute}}"
            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight bg-white
        @if($error) border-red-500 @else border-gray-500 @endif">
        <option value="">Выберите значение</option>
        @foreach($options as $option)
            <option value="{{$option->id}}"
                    @if($value == $option->id) selected @endif>
                {{$option->name}}
            </option>
        @endforeach
    </select>
    <p class="text-red-500 text-xs italic">{{$error}} </p>
</div>
