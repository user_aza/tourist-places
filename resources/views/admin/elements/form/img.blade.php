<div class="px-4 bg-transparent hover:bg-gray-400 py-2 hover:border-transparent rounded">
    <label for="{{$attribute}}">
        <input name="{{$attribute}}" type="checkbox" id="{{$attribute}}"/>
        {{ $label }}
        <img width="100" src="{{$src}}" alt="">
    </label>
</div>
