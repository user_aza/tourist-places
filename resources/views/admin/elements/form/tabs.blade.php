<div id="tabs-id">
    <ul class="flex p-3">
        @foreach(config('app.locales') as $locale => $value)
            <li class="text-center w-32  ">
                <a class="block font-bold rounded shadow-lg border-b-2 border-blue-700 @if($loop->iteration == 1) bg-blue-500 text-white @else text-blue-500 @endif"
                   data-locale="{{$locale}}" onclick="changeAtiveTab(event)">
                    {{$value['label']}}
                </a>
            </li>
        @endforeach
    </ul>
    <div class="relative flex flex-col w-full">
        <div class="tab-content tab-space">
            {{ $tab_content }}
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function changeAtiveTab(event){
            let element = event.target;
            while(element.nodeName !== "A"){
                element = element.parentNode;
            }
            ulElement = element.parentNode.parentNode;
            aElements = ulElement.querySelectorAll("li > a");
            tabContents = document.getElementById("tabs-id").querySelectorAll(".tab-content > div");
            for(let i = 0 ; i < aElements.length; i++){
                aElements[i].classList.remove("text-white");
                aElements[i].classList.remove("bg-blue-500");
                aElements[i].classList.add("text-blue-500");
                aElements[i].classList.add("bg-white");
                tabContents[i].classList.add("hidden");
                tabContents[i].classList.remove("block");
            }
            element.classList.remove("text-blue-500");
            element.classList.remove("bg-white");
            element.classList.add("text-white");
            element.classList.add("bg-blue-500");
            let selectedElement = document.getElementById(element.dataset.locale)
            selectedElement.classList.remove("hidden");
            selectedElement.classList.add("block");
        }
    </script>
@endpush
