<div class="p-3">
    <label class="text-gray-700 text-ms font-bold mb-2">{{$label}}</label>
    <textarea type="text" name="{{$attribute}}" placeholder="{{$placeholder}}"
              class="editor shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
        @if(isset($error) && $error) border-red-500 @else border-gray-500 @endif">{{$value}}</textarea>
    <p class="text-red-500 text-xs italic">@if(isset($error)){{$error}} @endif</p>
</div>

