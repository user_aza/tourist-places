{{--<div class="p-3">--}}
{{--    <label class="text-gray-700 text-ms font-bold mb-2">{{$label}}</label>--}}
{{--    <textarea type="text" name="{{$attribute}}" placeholder="{{$placeholder}}" id="editor"--}}
{{--              class="shadow appearance-none border border-teal-600 rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight--}}
{{--           focus:outline-none focus:shadow-outline">{{$value}}--}}
{{--    </textarea>--}}
{{--</div>--}}

<div class="p-3">
    <label class="text-gray-700 text-ms font-bold mb-2">Описание</label>
    <textarea name="description" id="editor" placeholder="Описание" id="editor"
              class="shadow appearance-none border border-gray-500 rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight"
    >@if(isset($user) && $user->author){{$user->author->description}}@endif</textarea>
</div>
