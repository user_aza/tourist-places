<div class="flex justify-between mb-5">
    <p class="ml-3 px-3 text-gray-700 text-lg font-bold">{{$title}}</p>
    <a class="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-2 border-blue-700 hover:border-blue-500 rounded"
       href="{{ url()->previous() }}">Назад</a>
</div>
