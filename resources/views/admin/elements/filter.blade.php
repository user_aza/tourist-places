<form action="{{$data['action']}}" method="get" class="flex justify-start align-items-center flex-wrap">
    @foreach($data['fields'] as $field)
        @if($field['type'] == 'input')
            @if(is_array($field['label']))
                @foreach($field['label'] as $label => $value)
                    <input autocomplete="off" type="text" name="{{$field['attribute']}}[]"
                           value="" placeholder="{{$label}}"
                           class="border border-teal-600 rounded px-2 mx-3 my-1 w-40">
                @endforeach
            @else
                <input autocomplete="off" type="text" name="{{$field['attribute']}}"
                       value="{{request()->query($field['attribute'])}}" placeholder="{{$field['label']}}"
                       class="border border-teal-600 rounded px-2 mx-3 my-1 w-40">
            @endif

        @elseif($field['type'] == 'select')
            <select name="{{$field['attribute']}}" class="px-2 rounded w-40 mx-3 my-1 border border-teal-600 bg-white">
                <option value="all">{{$field['label']}}</option>
                @foreach($field['options'] as $key => $option)
                    <option value="{{$key}}"
                            @if(is_numeric( request()->query($field['attribute'])) && $key == request()->query($field['attribute'])) selected @endif >
                        {{$option}}
                    </option>
                @endforeach
            </select>
        @endif
    @endforeach
    <input type="submit" value="Найти"
           class="btn bg-blue-500 hover:bg-blue-400 text-white rounded px-3 ml-3 my-1">
    <a href="{{$data['resetUrl']}}"
       class="btn bg-red-600 hover:bg-red-400 text-white rounded px-3 ml-3 my-1">Сброс</a>
</form>
