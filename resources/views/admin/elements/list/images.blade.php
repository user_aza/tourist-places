<div class="flex items-center flex-wrap">
    @foreach($images as $image)
        <div class="col-md-8 m-2">
            <img width="100" src="{{$image->src}}"/>
            <a href="{{route('admin.media.delete', ['id' => $image->id])}}">Удалить</a>
        </div>
    @endforeach
</div>
