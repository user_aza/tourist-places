<select name="{{$data->attribute}}" class="px-2 rounded w-40 mx-3 my-1 border border-gray-500 bg-white">
    <option value="">{{$data->label}}</option>
    @foreach($data->options as $key => $option)
        <option value="{{$key}}">
            {{$option}}
        </option>
    @endforeach
</select>
