@foreach($data->label as $label)
    <input autocomplete="off" type="text" name="{{$data->attribute}}[]"
           value="" placeholder="{{$label}}"
           class="border border-gray-500 rounded px-2 mx-3 my-1 w-40">
@endforeach
