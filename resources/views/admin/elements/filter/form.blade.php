<form action="{{$submitRoute}}" method="get" class="flex justify-start align-items-center flex-wrap">
    {!! $formElements !!}
    <button type="submit" value="Найти"
           class="btn bg-blue-500 hover:bg-blue-400 text-white rounded px-3 ml-3 my-1"> <i class="fas fa-search"></i>
    </button>
    <a href="{{$resetRoute}}"
       class="btn bg-red-600 hover:bg-red-400 text-white rounded px-3 ml-3 my-1">
        <i class="fas fa-reply"></i>
    </a>
</form>
