<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="{{asset('ckeditor5-build-classic/ckeditor.js')}}"></script>
    <script src="{{asset('js/font-awesome.js')}}"></script>
</head>

<body class=" font-sans leading-normal tracking-normal mt-12">
<!--Nav-->

<nav class="header-nav shadow-lg mt-0 fixed w-full z-20 top-0">
    <div class="flex justify-between items-center ">
        <div class="flex flex-shrink md:w-1/3 justify-center md:justify-start text-white font-bold p-4">
            <a href="{{route('admin.dashboard')}}"> Admin</a>
        </div>

        <div class="flex w-full pt-2 content-center justify-between md:w-1/3 md:justify-end">
            <ul class="list-reset flex justify-between flex-1 md:flex-none items-center">
                <li class="flex-1 md:flex-none md:mr-3">
                    <div class="relative inline-block pl-16">
                        <button onclick="toggleDD('myDropdown')"
                                class="drop-button text-white focus:outline-none hover:text-gray-400 text-lg"><span
                                class="pr-2"><i class="em em-robot_face"></i></span> {{auth()->guard()->user()->name}}
                            <svg class="h-5 fill-current inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                            </svg>
                        </button>
                        <div id="myDropdown"
                             class="dropdownlist absolute bg-gray-800 text-white right-0 mt-5 p-3 overflow-auto z-30 invisible">
                            <a href="#"
                               class="p-2 hover:bg-gray-700 text-white text-sm no-underline hover:no-underline block"><i
                                    class="fa fa-user fa-fw"></i> Profile</a>
                            <a href="#"
                               class="p-2 hover:bg-gray-700 text-white text-sm no-underline hover:no-underline block"><i
                                    class="fa fa-cog fa-fw"></i> Settings</a>
                            <div class="border border-gray-800"></div>
                            <a href="{{route('admin.auth.logout')}}"
                               class="p-2 hover:bg-gray-700 text-white text-sm no-underline hover:no-underline block"><i
                                    class="fas fa-sign-out-alt fa-fw"></i> Log Out</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="flex flex-col md:flex-row">
    <div class="sidebar fixed bottom-0 md:relative md:h-screen z-10 w-full md:w-48">
        <div
            class="md:mt-12 md:w-48 md:fixed md:left-0 md:top-0 content-center md:content-start text-left justify-between">
            <ul class="list-reset flex flex-row md:flex-col py-0 md:py-3 px-1 md:px-2 text-center md:text-left">
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.dashboard')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fas fa-tachometer-alt mr-3"></i>
                        Панель
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.user')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fa fa-user-circle mr-3" aria-hidden="true"></i>
                        Пользователи
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.role')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fab fa-r-project mr-3"></i>
                        Роли
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.permission')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fas fa-user-lock mr-3"></i>
                        Права
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.event')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fas fa-calendar-week mr-4"></i>
                        События
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.place')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fas fa-map-marked-alt mr-3"></i>
                        Места
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.tag')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fas fa-tags mr-3"></i>
                        Теги
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.faq')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="far fa-question-circle mr-3"></i>
                        Вопросы
                    </a>
                </li>
                <li class="mr-3 flex-1">
                    <a href="{{route('admin.media.index')}}"
                       class="block p-2 align-middle text-white hover:text-gray-400 text-sm font-bold">
                        <i class="fas fa-photo-video mr-3"></i>
                        Медиа
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-content flex-1 bg-gray-100 mt-12 md:mt-2 p-5" id="app">

        <div class="container mx-auto">
            <div class="overflow-x-auto">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/app.js') }}"></script>


<script>
    // var firebaseConfig = {
    //     apiKey: "AIzaSyDsGqQwpXap0W7-cPok7hIsMuZvvw4RmFo",
    //     authDomain: "tourist-195af.firebaseapp.com",
    //     databaseURL: "https://tourist-195af.firebaseio.com",
    //     projectId: "tourist-195af",
    //     storageBucket: "tourist-195af.appspot.com",
    //     messagingSenderId: "496488124087",
    //     appId: "1:496488124087:web:76d2bc6856276e00f4c732"
    // };
    // firebase.initializeApp(firebaseConfig);
    //
    // const messaging = firebase.messaging();
    // messaging.usePublicVapidKey("BEARuaXmDcCInTp1VHd3GbiiOJiU939lj4JjUUPff77vRqD3mPkSU3Jz9lWVzdUIE-zSo78_dsSy4zDHBpXnedM");
    //
    // function sendTokenToServer(fcm_token) {
    //     fetch('/admin/users/save-token/' + fcm_token)
    //         .then(response => response.json())
    //         .then(res => console.log(res))
    // }
    //
    // function retrieveToken() {
    //     messaging.getToken().then((fcm_token) => {
    //         if (fcm_token) {
    //             console.log('Token received :' + fcm_token);
    //             sendTokenToServer(fcm_token);
    //         } else {
    //             alert('You should allow notification!')
    //         }
    //     }).catch((err) => {
    //         console.log(err.message)
    //     });
    // }
    //
    // retrieveToken();
    //
    // messaging.onTokenRefresh(() => {
    //     retrieveToken()
    // });
    //
    //
    // messaging.onMessage((payload) => {
    //     console.log('Message received.');
    //     console.log(payload);
    //
    //     // location.reload();
    // });

    ClassicEditor
        .create(document.querySelector('.editor'), {
            toolbar: [
                'heading', '|',
                'bold',
                'italic', '|',
                'numberedList',
                'bulletedList', '|',
                'indent',
                'outdent', '|',
                'link',
                'blockQuote', '|',
                'Undo',
                'Redo'
            ],
        })
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error();
        });


    function toggleDD(myDropMenu) {
        document.getElementById(myDropMenu).classList.toggle("invisible");
    }

    function filterDD(myDropMenu, myDropMenuSearch) {
        var input, filter, ul, li, a, i;
        input = document.getElementById(myDropMenuSearch);
        filter = input.value.toUpperCase();
        div = document.getElementById(myDropMenu);
        a = div.getElementsByTagName("a");
        for (i = 0; i < a.length; i++) {
            if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                a[i].style.display = "";
            } else {
                a[i].style.display = "none";
            }
        }
    }

    // Close the dropdown menu if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.drop-button') && !event.target.matches('.drop-search')) {
            var dropdowns = document.getElementsByClassName("dropdownlist");
            for (var i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (!openDropdown.classList.contains('invisible')) {
                    openDropdown.classList.add('invisible');
                }
            }
        }
    }
</script>
@stack('scripts')
</body>
</html>
