@extends('admin.layout.master')
@section('content')
    <div class="container">
        <div class="flex justify-between mb-5 bg">
            <p class="ml-3 px-3 text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
                href="{{ route('admin.faq.create') }}">
                <i class="far fa-plus-square"></i>
            </a>
        </div>
        <table class="w-full text-left table-collapse">
            <thead class="thead">
            <tr>
                <th class="p-2">Id</th>
                <th class="p-2">Вопрос</th>
                <th class="p-2">Ответ</th>
                <th class="p-2 text-right pr-12">Действия</th>
            </tr>
            </thead>
            <tbody class="align-baseline">
            @foreach($faqs as $faq)
                <tr>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$faq->id}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {{$faq->question}}
                    </td>
                    <td class="p-2 border-t border-gray-300 font-mono text-xs">
                        {!! $faq->answer !!}
                    </td>
                    <td class="p-3 border-t border-gray-300 font-mono text-xs font-bold text-gray-700 mr-16 text-right">
                        <div class="inline-flex">
                            <a href="{{ route('admin.faq.edit', ['id' => $faq->id]) }}"
                               class="pr-10">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="{{ route('admin.faq.delete', ['id' => $faq->id]) }}"
                               class="pr-10">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $faqs->appends(request()->all())->links() }}
    </div>
@endsection
