@extends('admin.layout.master')
@section('content')
    <div>
        <div class="flex justify-between mb-5">
            <p class="text-gray-700 text-lg font-bold">{{$title}}</p>
            <a class="pr-10"
               href="{{ url()->previous() }}">
                <i class="fas fa-step-backward"></i>
            </a>
        </div>

        <form action="{{$action}}" method="post" enctype="multipart/form-data">
            @csrf
            <div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Вопрос</label>
                    <input type="text" name="question" placeholder="Вопрос"
                           value="@if(old('question')){{old('question')}}@elseif(isset($faq)){{$faq->question}}@endif"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                           @if($errors->first('question')) border-red-500 @else border-gray-500 @endif">
                    <p class="text-red-500 text-xs italic">{{$errors->first('question')}} </p>
                </div>
                <div class="p-3">
                    <label class="text-gray-700 text-ms font-bold mb-2">Ответ</label>
                    <textarea type="text" name="answer" placeholder="Ответ"
                              class="editor shadow appearance-none border rounded w-full py-2 px-3 text-gray-700  mb-1 leading-tight
                              @if($errors->first('answer')) border-red-500 @else border-gray-500 @endif">@if(isset($faq)){{$faq->answer}}@endif
                    </textarea>
                    <p class="text-red-500 text-xs italic">{{$errors->first('answer')}}</p>
                </div>
                <div class="p-3">
                    <input type="submit"
                           class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                           value="{{$submitName}}">
                </div>
            </div>
        </form>
    </div>
@endsection

