<?php

namespace App\Console\Commands;

use App\Role;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class AddAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('Введите имя?');
        $login = $this->ask('Введите login?');
        $email = $this->ask('Введите email?');
        $password = $this->secret('Введите пароль?');


        $data = [
            'name' => $name,
            'login' => $login,
            'email' => $email,
            'password' => $password,
            'active' => 1,
            'role_id' => Role::query()->where('name', 'Админ')->first()->id

        ];

        $validator = Validator::make($data, [
            'name' => 'required',
            'login' => 'required|unique:users',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8'
        ], [
            'required' => ':attribute обязательно к заполнению.',
            'min' => ':attribute должен содержать минимум :min символов.',
            //'login' => ':attribute должен содержать цифры и буквы нижнего регистра.',
            'email' => ':attribute должен быть email-адресом.',
            'unique' => 'Данный :attribute уже зарегистриован.',
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->toArray() as $error) {
                $this->error($error[0]);
            }
        } else {
            if ($this->confirm('Сохранить данные?')) {


                User::create($data);
                $this->info("Админ успешно сохранен!");
            } else {
                $this->error("Вы отменили добавление админа");
            }
        }
    }
}
