<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static get()
 * @method static create($all)
 * @method static find($id)
 */
class Faq extends Model
{
    protected $table = 'faqs';
    protected $dates = [
        'updated_at',
        'created_at',
    ];
    protected $fillable = [
        'id',
        'question',
        'answer',
    ];
}
