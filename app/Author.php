<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static get()
 * @method static create(array $all)
 * @method static find($id)
 */
class Author extends Model
{
    protected $table = 'authors';
    protected $dates = [
        'updated_at',
        'created_at',
    ];
    protected $fillable = [
        'id',
        'user_id',
        'description',
        'address',
        'phone',
    ];

    public $timestamps = false;

    public function events(){
        return $this->hasMany(Event::class, 'event_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function medias()
    {
        return $this->morphToMany(Media::class, 'mediables');
    }

}
