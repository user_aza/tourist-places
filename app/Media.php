<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static latest()
 * @method static get()
 */
class Media extends Model
{
    protected $table = 'medias';
    protected $dates = [
        'updated_at',
        'created_at',
    ];
    protected $fillable = [
        'id',
        'src',
        'main'
    ];

    public function getSrcAttribute($src)
    {
        if ($src){
            return asset('storage/'.$src);
        } else {
            return $src;
        }
    }

    public function places()
    {
        return $this->morphToMany(Place::class, 'mediables');
    }

    public function events()
    {
        return $this->morphToMany(Event::class, 'mediables');
    }
    public function authors()
    {
        return $this->morphToMany(Author::class, 'mediables');
    }
}
