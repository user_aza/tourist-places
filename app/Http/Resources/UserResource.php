<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource->toArray();
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'login' => $this->login,
            'email' => $this->email,
            'active' => $this->active,
            'allow_notifications' => $this->allow_notifications,
            'fcm_token' => $this->fcm_token,
            'avatar' => '',
            'bg_image' => '',
        ];

        if (isset($resource['user_subscribes'])){
            $data['is_subscribed'] =  $this->userSubscribes->count() ?  !!$this->userSubscribes->first()->sub_scribed : false;
        }

        if (isset($resource['author'])){
            $data['address'] = $this->author ? $this->author->address : '';
            $data['phone'] = $this->author ? $this->author->phone : '';
            $data['description'] = $this->author ? $this->author->description : '';
        }

        if (isset($resource['medias'])){
            foreach ($this->medias as $media){
                if ($media->main){
                    $data['avatar'] = $media->src;
                } else {
                    $data['bg_image'] = $media->src;
                }
            }
        }

        if (isset($resource['events'])){
            $data['events'] = EventResource::collection($this->events);
        }


        return $data;
    }
}
