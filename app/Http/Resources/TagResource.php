<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource->toArray();
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
        ];

        if (isset($resource['user_subscribes'])){
            $data['is_subscribed'] =  $this->userSubscribes->count() ?  ($this->userSubscribes->first()->sub_scribed == true) : false;
        }

        return $data;
    }
}
