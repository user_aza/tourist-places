<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource->toArray();
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'images' => $this->medias->pluck('src'),
        ];

        if (isset($resource['user_favorites'])){
            $data['is_favorite'] =  $this->userFavorites->count() ?  ($this->userFavorites->first()->li_ke > 0) : false;
        }

        if (isset($resource['user_subscribes'])){
            $data['is_subscribed'] =  $this->userSubscribes->count() ?  ($this->userSubscribes->first()->sub_scribed == true) : false;
        }

        if (isset($resource['nearestEvents'])){
            $data['nearest_events'] = EventResource::collection($this->nearestEvents);
        }


        return $data;
    }
}
