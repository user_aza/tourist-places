<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource->toArray();
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'description' => $this->description,
            'period' => $this->period,
            'start_date_time' => $this->start_date_time,
            'start_place' => $this->start_place,
            'active' => $this->active,
            'created_at' => $this->created_at->format('d-m-Y H:m:s'),
        ];

        if (isset($resource['user_favorites'])){
            $data['is_favorite'] =  $this->userFavorites->count() ?  ($this->userFavorites->first()->li_ke > 0) : false;
        }
        if (isset($resource['medias'])){
            $data['image'] = $this->medias->count() ? $this->medias->first()->src : '';
        }
        if (isset($resource['user'])){
            $data['author'] = UserResource::make($this->user);
        }
        if (isset($resource['places'])){
            $data['places'] = PlaceResource::collection($this->places);
        }


        return $data;
    }
}
