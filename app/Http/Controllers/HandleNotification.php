<?php


namespace App\Http\Controllers;


use App\Place;
use App\Services\LaravelFCM\Facades\FCM;
use App\Services\LaravelFCM\Message\OptionsBuilder;
use App\Services\LaravelFCM\Message\PayloadDataBuilder;
use App\Services\LaravelFCM\Message\PayloadNotificationBuilder;
use App\Subscribe;
use App\Tag;
use App\User;

trait HandleNotification
{
    private function notification($channel, $event, $subscribableType)
    {
        $subscribes = Subscribe::with('user', 'subscribable')
            ->whereHas('user', function ($q) {
                $q->where('allow_notifications', 1);
                $q->whereNotNull('fcm_token');
            })
            ->where('subscribable_type', $subscribableType)
            ->where('sub_scribed', 1)
            ->get();

        $data = [];
        foreach ($subscribes as $subscribe) {
            $data[$subscribe->subscribable->id]['title'] = $subscribe->subscribable->getNotyTitle($subscribe->subscribable);
            $data[$subscribe->subscribable->id]['body'] = $subscribe->subscribable->getNotyBody($subscribe->subscribable);
            $data[$subscribe->subscribable->id]['fcm_tokens'][] = $subscribe->user->fcm_token;
        }

        foreach ($data as $datum) {
            $this->broadcastMessage(
                $channel,
                $event->id,
                $datum['fcm_tokens'],
                $datum['title'],
                $datum['body']
            );
        }
    }

    private function notificationByPlace($place)
    {
        $userIds = $place->subscribes->pluck('user_id');
        $this->broadcastMessage('place_details_screen', $place->id, $this->getUserTokens($userIds));
    }

    private function broadcastMessage($channel, $id, $userFcmTokens, $title = '', $body = '')
    {
        info($channel, [$id, $userFcmTokens, $title, $body]);
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setSound('default')
            ->setClickAction('FLUTTER_NOTIFICATION_CLICK');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            'id' => $id,
            'rout_id' => $channel,
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($userFcmTokens, $option, $notification, $data);
        return $downstreamResponse->numberSuccess();
    }

    private function getUserTokens($userIds)
    {
        $userFcmTokens = User::query()
            ->whereIn('id', $userIds)
            ->where('allow_notifications', true)
            ->whereNotNull('fcm_token')
            ->pluck('fcm_token', 'login');
        return $userFcmTokens->count() ? $userFcmTokens->toArray() : null;
    }
}
