<?php


namespace App\Http\Controllers\Admin;


use App\Author;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleNotification;
use App\Http\Controllers\MediaHandler;
use App\Http\Requests\UserRequest;
use App\Role;
use App\Services\Filters\Equal;
use App\Services\Filters\Filter;
use App\Services\Filters\Like;
use App\Services\LaravelFCM\Facades\FCM;
use App\Services\LaravelFCM\Message\OptionsBuilder;
use App\Services\LaravelFCM\Message\PayloadDataBuilder;
use App\Services\LaravelFCM\Message\PayloadNotificationBuilder;
use App\User;

class UserController extends Controller
{
    use MediaHandler;
    use HandleNotification;

    public function index()
    {
        $query = User::with('role', 'medias', 'author.medias');
        $filter = new Filter($query, request()->all());
        $filter->addField('name', 'Имя', new Like());
        $filter->addField('login', 'Login', new Like());
        $filter->addField('email', 'Email', new Like());
        $filter->addField('role_id', 'Роль', new Equal(Role::pluck('name', 'id')));
        $filter->addField('active', 'Все', new Equal([1 => 'Да', 0 => 'Нет']));
        $filterForm = $filter->getForm(route('admin.user', request()->all()), route('admin.user'));
        $users = $query->paginate(10);
        return view('admin.user.index', [
            'users' => $users,
            'title' => 'Пользователи',
            'filterForm' => $filterForm
        ]);
    }

    public function create()
    {
        $roles = Role::get();
        return view('admin.user.form', [
            'title' => 'Добавление пользователя',
            'roles' => $roles,
            'action' => route('admin.user.store'),
            'submitName' => 'Сохранить'
        ]);
    }

    public function store(UserRequest $request)
    {
        $user = User::create([
            'name' => $request->get('name'),
            'login' => $request->get('login'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'active' => $request->has('active'),
            'role_id' => $request->get('role_id'),
            'allow_notifications' => 1,
        ]);

        $this->createMedia($request->file('src'), $user, 1);
        $this->createMedia($request->file('bg'), $user);
        if ($request->get('role_id') != 1) {
            Author::query()->create([
                'user_id' => $user->id,
                'description' => $request->get('description'),
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
            ]);
        }

        return redirect()->route('admin.user');
    }

    public function edit($id)
    {
        $user = User::with('author', 'medias')->find($id);
        $roles = Role::get();
        return view('admin.user.form', [
            'title' => 'Редактирование пользователя',
            'user' => $user,
            'roles' => $roles,
            'action' => route('admin.user.update', ['id' => $id]),
            'submitName' => 'Изменить'
        ]);
    }

    public function update($id, UserRequest $request)
    {
        $user = User::with('medias', 'author.medias')->find($id);
        $request->merge(['active' => $request->has('active')]);
        $user->update($request->all());

        if ($request->file('src') || $request->file('bg')) {
            foreach ($user->medias as $media) {
                if ($media->main && $request->file('src')) {
                    $this->deleteImageWithDetach($media->id);
                }
                if (!$media->main && $request->file('bg')) {
                    $this->deleteImageWithDetach($media->id);
                }
            }
            $this->createMedia($request->file('src'), $user, 1);
            $this->createMedia($request->file('bg'), $user);
        }
        return redirect()->route('admin.user');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admin.user');
    }

    public function testFcmMessage()
    {
        $tokens = User::query()->whereNotNull('fcm_token')->pluck('fcm_token', 'login');
        $this->broadcastMessage('place_details_screen', 1, $tokens->toArray());
        return $tokens;
    }
}
