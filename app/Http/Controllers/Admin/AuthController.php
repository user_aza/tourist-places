<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\UserLoginJob;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function form()
    {
        return view('admin.auth.form');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ], [
            'required' => 'Поле обязательно к заполнению.',
            'min' => 'Поле должен содержать минимум :min символов.',
            'email' => 'Поле должен быть email-адресом.',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.auth.form')
                ->withErrors($validator)
                ->withInput();
        }

        if (Auth::guard()->attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'active' => 1
        ])) {
//            UserLoginJob::dispatch(User::find(Auth::id()));
            return redirect()->intended('admin');
        } else {
            $validator->errors()->add('auth_fail', trans('auth.failed'));

            return redirect()->guest(route('admin.auth.form'))
                ->withInput($request->only('email'))
                ->withErrors($validator);
        }
    }

    public function logout()
    {
        Auth::guard()->logout();
        return redirect()->intended('admin');
    }
}
