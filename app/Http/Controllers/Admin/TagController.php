<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Place;
use App\Services\Filters\Filter;
use App\Services\Filters\Like;
use App\Tag;

class TagController extends Controller
{
    public function index()
    {
        $query = Tag::with('places', 'events');
        $filter = new Filter($query, request()->all());
        $filter->addField('name', 'Имя', new Like());
        $filterForm = $filter->getForm(route('admin.tag', request()->all()), route('admin.tag'));
        $tags = $query->paginate(10);
        return view('admin.tag.index', [
            'tags' => $tags,
            'title' => 'Теги',
            'filterForm' => $filterForm
        ]);
    }

    public function create()
    {
        $places = Place::query()->get();
        $events = Event::query()->get();
        return view('admin.tag.form', [
            'title' => 'Добавление тега',
            'places' => $places,
            'events' => $events,
            'action' => route('admin.tag.store'),
            'submitName' => 'Сохранить'
        ]);
    }

    public function store(TagRequest $request)
    {
        Tag::create($request->all());
        return redirect()->route('admin.tag');
    }

    public function edit($id)
    {
        $tag = Tag::find($id);
        $events = Event::query()->get();
        $places = Place::query()->get();
        return view('admin.tag.form', [
            'title' => 'Редактирование тега',
            'tag' => $tag,
            'places' => $places,
            'events' => $events,
            'action' => route('admin.tag.update', ['id' => $id]),
            'submitName' => 'Изменить'
        ]);
    }

    public function update($id, TagRequest $request)
    {
        $tag = Tag::find($id);
        $tag->update($request->all());
        return redirect()->route('admin.tag');
    }

    public function delete($id)
    {
        $tag = Tag::find($id);
        $tag->delete();
        return redirect()->route('admin.tag');
    }
}
