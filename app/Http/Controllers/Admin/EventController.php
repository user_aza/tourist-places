<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleNotification;
use App\Http\Controllers\HandleTagables;
use App\Http\Controllers\MediaHandler;
use App\Http\Requests\EventRequest;
use App\Place;
use App\Services\Filters\Filter;
use App\Services\Filters\Like;
use App\Tag;
use App\User;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    use MediaHandler;
    use HandleTagables;
    use HandleNotification;

    public function index()
    {
        $query = Event::with('user', 'places', 'tags');
        $filter = new Filter($query, request()->all());
        $filter->addField('name', 'Имя', new Like());
        $filterForm = $filter->getForm(route('admin.event', request()->all()), route('admin.event'));
        $events = $query->paginate(10);
        return view('admin.event.index', [
            'events' => $events,
            'title' => 'События',
            'filterForm' => $filterForm
        ]);
    }

    public function create()
    {
        $places = Place::query()->get();
        $tags = Tag::get();
        $users = User::get();
        return view('admin.event.form', [
            'title' => 'Добавление события',
            'places' => $places,
            'tags' => $tags,
            'users' => $users,
            'action' => route('admin.event.store'),
            'submitName' => 'Сохранить'
        ]);
    }

    public function store(EventRequest $request)
    {
        $request->merge([
            'active' => $request->has('active')
        ]);
        $event = Event::create([
            'name' => $request->get('name'),
            'price' => $request->get('price'),
            'description' => $request->get('description'),
            'period' => $request->get('period'),
            'start_date_time' => $request->get('start_date_time'),
            'start_place' => $request->get('start_place'),
            'user_id' => $request->get('user_id'),
            'active' => $request->get('active')
        ]);

        $this->createMedia($request->file('src'), $event);

        if (is_array($request->get('placeIds'))) {
            $event->places()->sync($request->get('placeIds'));
            if ($event->active) {
                $this->notification('place_details_screen', $event, Place::class);
            }
        }

        if (is_array($request->get('tagIds'))) {
            $this->saveTags($event, $request->get('tagIds'));
            if ($event->active) {
                $this->notification('event_details_screen', $event, Tag::class);
            }
        }
        return redirect()->route('admin.event');
    }

    public function edit($id)
    {
        $event = Event::with('places', 'medias', 'user')->find($id);

        $places = Place::get();
        $tags = Tag::get();
        $users = User::get();
        return view('admin.event.form', [
            'title' => 'Редактирование события',
            'event' => $event,
            'places' => $places,
            'tags' => $tags,
            'users' => $users,
            'action' => route('admin.event.update', ['id' => $id]),
            'submitName' => 'Изменить'
        ]);
    }

    public function update($id, EventRequest $request)
    {
        $request->merge([
            'active' => $request->has('active')
        ]);
        $event = Event::query()->findOrFail($id);
        $oldActive = $event->active;
        $event->update($request->all());
        $this->createMedia($request->file('src'), $event);

        if (!$oldActive && $request->has('active')) {
            $this->notification('author_details_screen', $event, User::class);
        }

        if (is_array($request->get('placeIds'))) {
            $event->places()->sync($request->get('placeIds'));
            if ($request->has('active')) {
                $this->notification('place_details_screen', $event, Place::class);
            }
        }

        if (is_array($request->get('tagIds'))) {
            $this->saveTags($event, $request->get('tagIds'));
            if ($request->has('active')) {
                $this->notification('event_details_screen', $event, Tag::class);
            }
        }

        return redirect()->route('admin.event');
    }

    public function delete($id)
    {
        $event = Event::query()->find($id);
        DB::table('event_places')->where([
            'event_id' => $event->id,
        ])->delete();
        $event->delete();
        return redirect()->route('admin.event');
    }
}
