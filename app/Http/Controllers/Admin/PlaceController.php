<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleNotification;
use App\Http\Controllers\HandleTagables;
use App\Http\Controllers\MediaHandler;
use App\Http\Requests\PlaceRequest;
use App\Media;
use App\Place;
use App\Services\Filters\Filter;
use App\Services\Filters\Like;
use App\Tag;

class PlaceController extends Controller
{
    use MediaHandler;
    use HandleTagables;
    use HandleNotification;

    public function index()
    {
        $query = Place::with('medias', 'events', 'tags');
        $filter = new Filter($query, request()->all());
        $filter->addField('name', 'Имя', new Like());
        $filterForm = $filter->getForm(route('admin.place', request()->all()), route('admin.place'));
        $places = $query->paginate(10);
        return view('admin.place.index', [
            'places' => $places,
            'title' => 'Места',
            'filterForm' => $filterForm
        ]);
    }

    public function create()
    {
        $medias = Media::latest()->get();
        $events = Event::get();
        $tags = Tag::get();
        return view('admin.place.form', compact('medias'), [
            'title' => 'Добавление места',
            'medias' => $medias,
            'events' => $events,
            'tags' => $tags,
            'action' => route('admin.place.store'),
            'submitName' => 'Сохранить'
        ]);
    }

    public function store(PlaceRequest $request)
    {
        $place = Place::create($request->all());
        $this->createMedia($request->file('src'), $place);
        if (is_array($request->get('eventIds'))) {
            $place->events()->sync($request->get('eventIds'));
        }
        if (is_array($request->get('tagIds'))) {
            $this->saveTags($place, $request->get('tagIds'));
        }
        return redirect()->route('admin.place');
    }

    public function edit($id)
    {
        $place = Place::with('medias')->findOrFail($id);
        $events = Event::get();
        $tags = Tag::get();
        $ownMediaIds = $place->medias->pluck('id');
        $medias = Media::query()->whereNotIn('id', $ownMediaIds)->get();
        return view('admin.place.form', [
            'title' => 'Редактиование места',
            'action' => route('admin.place.update', ['id' => $id]),
            'submitName' => 'Изменить',
            'place' => $place,
            'tags' => $tags,
            'events' => $events,
            'medias' => $medias,
        ]);
    }

    public function update($id, PlaceRequest $request)
    {
        $place = Place::query()->findOrFail($id);
        $place->update($request->all());
        $this->createMedia($request->file('src'), $place);
        if (is_array($request->get('eventIds'))) {
            $place->events()->sync($request->get('eventIds'));
        }
        if (is_array($request->get('tagIds'))) {
            $this->saveTags($place, $request->get('tagIds'));
        }
        $this->notificationByPlace($place);

        return redirect()->route('admin.place');
    }

    public function delete($id)
    {
        $place = Place::find($id);
        $place->delete();
        return redirect()->route('admin.place');
    }
}
