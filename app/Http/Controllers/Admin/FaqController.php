<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqRequest;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::query()->paginate(10);
        return view('admin.faq.index', [
            'faqs' => $faqs,
            'title' => 'Вопросы и ответы',
        ]);
    }

    public function create()
    {
        $faqs = Faq::get();
        return view('admin.faq.form', [
            'title' => 'Добавление вопросов и ответ',
            'action' => route('admin.faq.store'),
            'submitName' => 'Сохранить',
            'faqs' => $faqs,

        ]);
    }

    public function store(FaqRequest $request)
    {
        Faq::create($request->all());
        return redirect()->route('admin.faq');
    }

    public function edit($id)
    {
        $faq = Faq::find($id);
        return view('admin.faq.form', [
            'title' => 'Редактирование вопросов и ответ',
            'faq' => $faq,
            'action' => route('admin.faq.update', ['id' => $id]),
            'submitName' => 'Изменить'
        ]);
    }

    public function update($id, FaqRequest $request)
    {
        $faq = Faq::find($id);
        $faq->update($request->all());
        return redirect()->route('admin.faq');
    }

    public function delete($id)
    {
        $faq = Faq::find($id);
        $faq->delete();
        return redirect()->route('admin.faq');
    }
}
