<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Role;
use App\Permission;
use App\Services\Filters\Filter;
use App\Services\Filters\Has;
use App\Services\Filters\Like;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $query = Role::with('permissions');
        $filter = new Filter($query, request()->all());
        $filter->addField('name', 'Имя', new Like());
        $filter->addField('description', 'Описание', new Like());
        $filter->addField('permission_id', 'Права', new Has('permissions', Permission::pluck('name', 'id')));
        $filterForm = $filter->getForm(route('admin.role', request()->all()), route('admin.role'));
        $roles = $query->paginate(10);

        return view('admin.role.index', [
            'roles' => $roles,
            'title' => 'Роль',
            'filterForm' => $filterForm
        ]);
    }

    public function create()
    {
        $permissions = Permission::query()->get();
        return view('admin.role.form', [
            'title' => 'Добавление роли',
            'permissions' => $permissions,
            'action' => route('admin.role.store'),
            'submitName' => 'Сохранить'
        ]);
    }

    public function store(RoleRequest $request)
    {
        $role = Role::create($request->all());
        $role->permissions()->sync(request()->get('permissions'));
        return redirect()->route('admin.role');
    }

    public function edit($id)
    {
        $role = Role::with('permissions')->find($id);
        $permissions = Permission::get();
        return view('admin.role.form', [
            'title' => 'Редактирование роли',
            'role' => $role,
            'permissions' => $permissions,
            'action' => route('admin.role.update', ['id' => $id]),
            'submitName' => 'Изменить'
        ]);
    }

    public function update($id, RoleRequest $request)
    {
        $role = Role::find($id);
        $role->update($request->all());
        $role->permissions()->sync(request()->get('permissions'));
        return redirect()->route('admin.role');
    }

    public function delete($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->route('admin.role');
    }

}
