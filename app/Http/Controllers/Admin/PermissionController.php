<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Permission;
use App\Role;
use App\Services\Filters\Filter;
use App\Services\Filters\Has;
use App\Services\Filters\Like;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index()
    {
        $query = Permission::with('roles');
        $filter = new Filter($query, request()->all());
        $filter->addField('name', 'Имя', new Like());
        $filter->addField('role_id', 'Роль', new Has('roles', Role::pluck('name', 'id')));
        $filterForm = $filter->getForm(route('admin.permission', request()->all()), route('admin.permission'));
        $permissions = $query->paginate(10);

        return view('admin.permission.index', [
            'permissions' => $permissions,
            'title' => 'Права',
            'filterForm' => $filterForm
        ]);
    }


    public function create()
    {
        $roles = Role::get();
        return view('admin.permission.form', [
            'title' => 'Добавление права',
            'roles' => $roles,
            'action' => route('admin.permission.store'),
            'submitName' => 'Сохранить'
        ]);
    }

    public function store(PermissionRequest $request)
    {
        $permission = Permission::create($request->all());
        $permission->roles()->sync(request()->get('roles'));
        return redirect()->route('admin.permission');
    }

    public function edit($id)
    {
        $permission = Permission::find($id);
        $roles = Role::get();
        return view('admin.permission.form', [
            'title' => 'Редактирование права',
            'permission' => $permission,
            'roles' => $roles,
            'action' => route('admin.permission.update', ['id' => $id]),
            'submitName' => 'Изменить',
        ]);
    }

    public function update($id, PermissionRequest $request)
    {
        $permission = Permission::find($id);
        $permission->update($request->all());
        $permission->roles()->sync(request()->get('roles'));
        return redirect()->route('admin.permission');
    }

    public function delete($id)
    {
        $permission = Permission::find($id);
        $permission->delete();
        return redirect()->route('admin.permission');
    }
}
