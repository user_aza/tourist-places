<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MediaHandler;
use App\Media;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class MediaController extends Controller
{
    use MediaHandler;

    public function index()
    {
        $medias = Media::latest()->get();
        return view('admin.media.index', compact('medias'));
    }

    public function store(Request $request)
    {
//        $this->validate($request, [
//            'src' => 'image|required|mimes:jpeg,png,jpg,gif,svg'
//        ]);
        try {
            $this->storeMedia($request->file('src'));
            return back()->with('success', 'Ваши изображения были успешно загружены');
        }catch (\Exception $exception){
            return back()->withErrors(['error']);
        }
    }

    public function delete($id)
    {
        try {
            $this->deleteImageWithDetach($id);
            return back()->with('success', 'Ваши изображения были успешно удалены');
        }catch (\Exception $exception){
            return back()->withErrors(['error']);
        }
    }
}
