<?php

namespace App\Http\Controllers;

use App\Favorite;

trait HandleFavorite
{
    public function saveFavorite($favoritableId, $favoritablesType, $like,  $userId)
    {
        $favorite = Favorite::query()
            ->where('user_id', $userId)
            ->where('favoritable_id', $favoritableId)
            ->where('favoritable_type', $favoritablesType)
            ->first();

        if (!$favorite){
            $favorite = Favorite::query()->create([
                'user_id'           =>$userId,
                'favoritable_id'    => $favoritableId,
                'favoritable_type'  => $favoritablesType,
                'li_ke' => $like
            ]);
        } else {
            $favorite->li_ke = $like;
            $favorite->save();
        }
        return ($favorite->li_ke > 0) ? 'like' : 'dislike';
    }

    public static function deleteWishlist($favoritableId)
    {
        $result = Favorite::query()->where('user_id', auth()->id())
            ->where('favoritable_id', $favoritableId)
            ->where('favoritable_type', self::class)
            ->delete();
        return $result;
    }

    public function getFavoriteIds($userId, $favoritablesType)
    {
        return Favorite::query()
            ->where('user_id', $userId)
            ->where('favoritable_type', $favoritablesType)
            ->pluck('favoritable_id');
    }
}
