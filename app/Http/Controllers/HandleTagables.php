<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

trait HandleTagables
{
    public function saveTags($model, $newTagIds)
    {
        $oldTagIds = $model->tags()->pluck('id');
        foreach ($oldTagIds as $tagId){
            DB::table('taggables')->where([
                'tag_id' => $tagId,
                'taggable_id' => $model->id,
                'taggable_type' => get_class($model),
            ])->delete();
        }

        foreach ($newTagIds as $tagId){
            DB::table('taggables')->insert([
                'tag_id' => $tagId,
                'taggable_id' => $model->id,
                'taggable_type' => get_class($model),
            ]);
        }


    }


    public function getTaggableTypeIds($id, $type)
    {
        return DB::table('taggables')
            ->where('tag_id', $id)
            ->where('taggable_type', $type)
            ->pluck('taggable_id');
    }

}
