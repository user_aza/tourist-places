<?php


namespace App\Http\Controllers\Api;

use App\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;


class FaqController extends Controller
{
    /**
     * @OA\Get(
     *     path="/faq",
     *     operationId="questions",
     *     tags={"FAQ"},
     *     summary="questions",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="The page number",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     ),
     * )
     * @return JsonResponse
     */
    public function faq()
    {
        $faqs = Faq::query()->paginate();
        return response()->json($faqs, Response::HTTP_OK);
    }
}
