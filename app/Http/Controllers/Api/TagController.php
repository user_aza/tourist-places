<?php

namespace App\Http\Controllers\Api;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleNotification;
use App\Http\Controllers\HandleSubscribe;
use App\Http\Controllers\HandleTagables;
use App\Http\Resources\EventResource;
use App\Http\Resources\PlaceResource;
use App\Http\Resources\TagResource;
use App\Place;
use App\Tag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class TagController extends Controller
{
    use HandleTagables;
    use HandleSubscribe;
//    use HandleNotification;

    /**
     *  @OA\Get(
     *     path="/tags",
     *     operationId="tagId",
     *     tags={"Теги"},
     *     summary="page",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     *  )
     *
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $tags = Tag::with('userSubscribes')->get();
        return response()->json(TagResource::collection($tags)->response()->getData(true), Response::HTTP_OK);

    }

    /**
     * @OA\Get(
     *     path="/tags/{id}/places",
     *     operationId="placeId",
     *     tags={"Теги"},
     *     summary="page",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     *  )
     *
     * Display a listing of the resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function places($id)
    {
        $placeIds = $this->getTaggableTypeIds($id, Place::class);
        $places = Place::with('medias', 'userFavorites', 'userSubscribes')
            ->whereIn('id', $placeIds)
            ->paginate();
        return response()->json(PlaceResource::collection($places)->response()->getData(true), Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *     path="/tags/{id}/events",
     *     operationId="eventId",
     *     tags={"Теги"},
     *     summary="page",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     *  )
     *
     * Display a listing of the resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function events($id)
    {
        $eventIds = $this->getTaggableTypeIds($id, Event::class);
        $events = Event::with('medias', 'places.medias', 'user.medias', 'places.userFavorites', 'places.userSubscribes', 'userFavorites')
            ->whereIn('id', $eventIds)
            ->paginate();
        return response()->json(EventResource::collection($events)->response()->getData(true), Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/tags/{id}/subscribe",
     *     operationId="Places subscribed",
     *     tags={"Теги"},
     *     summary="subscribed",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="subscribed",
     *         in="query",
     *         description="boolean",
     *         required=false,
     *         @OA\Schema(
     *             type="boolean",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     * @param $id
     * @return JsonResponse
     */
    public function subscribe($id)
    {
        $tag = Tag::query()->findOrFail($id);
        $result = $this->saveSubscribe($tag->id, Tag::class, \request()->get('subscribed'), auth()->guard('api')->id());
//        $this->notification('event_details_screen', $event, $request->get('tagIds'), Tag::class);
        return response()->json(['type' => $result], Response::HTTP_OK);
    }
}
