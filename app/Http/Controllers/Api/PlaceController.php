<?php

namespace App\Http\Controllers\Api;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleFavorite;
use App\Http\Controllers\HandleSubscribe;
use App\Http\Controllers\MediaHandler;
use App\Http\Resources\EventResource;
use App\Http\Resources\PlaceResource;
use App\Place;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PlaceController extends Controller
{
    use MediaHandler;
    use HandleFavorite;
    use HandleSubscribe;

    /**
     * @OA\Get(
     *     path="/places",
     *     operationId="placeId",
     *     tags={"Места"},
     *     summary="page",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="The page number",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     *  )
     *
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */

    public function index()
    {
        $places = Place::with('medias', 'userFavorites', 'userSubscribes')->paginate();
        return response()->json(PlaceResource::collection($places)->response()->getData(true), Response::HTTP_OK);
    }


    /**
     * @OA\Get(
     *     path="/places/{id}",
     *     operationId="id",
     *     tags={"Места"},
     *     summary="id",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $place = Place::with(
            'medias',
            'userFavorites',
            'userSubscribes'
        )->findOrFail($id);

        $place->nearestEvents = Event::with('medias', 'places.medias', 'user.medias', 'places.userFavorites', 'places.userSubscribes', 'userFavorites')
            ->whereHas('places', function ($q) use ($id) {
                $q->where('place_id', $id);
            })
            ->whereDate('start_date_time', '>=', now())
            ->orderBy('start_date_time', 'asc')
            ->limit(10)
            ->get();
        return response()->json(PlaceResource::make($place), Response::HTTP_OK);
    }


    /**
     * @OA\Post(
     *     path="/places/{id}/favorite/{like}",
     *     operationId="Place favorite",
     *     tags={"Места"},
     *     summary="like",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="like",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     * @param $id
     * @param $like
     * @return JsonResponse
     */
    public function favorite($id, $like)
    {
        $place = Place::query()->findOrFail($id);
        $result = $this->saveFavorite($place->id, Place::class, $like, auth()->guard('api')->id());
        return response()->json(['type' => $result], Response::HTTP_OK);
    }


    /**
     * @OA\Get(
     *     path="/places/favorites",
     *     operationId="Places favorites",
     *     tags={"Места"},
     *     summary="place likes",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="The page number",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     */
    public function favorites()
    {
        $placeIds = $this->getFavoriteIds(auth()->guard('api')->id(), Place::class);
        $places = Place::with('userFavorites', 'userSubscribes')
            ->whereIn('id', $placeIds)
            ->paginate();

        return response()->json(PlaceResource::collection($places)->response()->getData(true), Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/places/{id}/subscribe",
     *     operationId="Places subscribed",
     *     tags={"Места"},
     *     summary="subscribed",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="subscribed",
     *         in="query",
     *         description="boolean",
     *         required=false,
     *         @OA\Schema(
     *             type="boolean",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     * @param $id
     * @return JsonResponse
     */
    public function subscribe($id)
    {
        $place = Place::query()->findOrFail($id);
        $result = $this->saveSubscribe($place->id, Place::class, \request()->get('subscribed'), auth()->guard('api')->id());
        return response()->json(['type' => $result], Response::HTTP_OK);
    }
}
