<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleFavorite;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * @OA\Info(
 *     title="Laravel Swagger API documentation",
 *     version="1.0.0",
 *     @OA\Contact(
 *         email="admin@example.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 * @OA\Tag(
 *     name="Auth",
 *     description="Регистрация/Авторизация",
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Demo API Server"
 * )
 *
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     name="Authorization",
 *     securityScheme="Bearer"
 * )
 *eV2itYjlp7Kdu54xYRXR-B:APA91bH22QI1AeAc1y8kQ5kRGEKyQBPbSN3cBWvPC_KkWaAtUZlRhxMI_RhtjvpX8bbzzPdbkQc4rG60AKwxAGwpyMSV9J2iPp6YR35wSSU9yQUV5gKpgh_2owjtPczSYGTzf1ODWkFc
 *eV2itYjlp7Kdu54xYRXR-B:APA91bH22QI1AeAc1y8kQ5kRGEKyQBPbSN3cBWvPC_KkWaAtUZlRhxMI_RhtjvpX8bbzzPdbkQc4rG60AKwxAGwpyMSV9J2iPp6YR35wSSU9yQUV5gKpgh_2owjtPczSYGTzf1ODWkFc
 *eV2itYjlp7Kdu54xYRXR-B:APA91bH22QI1AeAc1y8kQ5kRGEKyQBPbSN3cBWvPC_KkWaAtUZlRhxMI_RhtjvpX8bbzzPdbkQc4rG60AKwxAGwpyMSV9J2iPp6YR35wSSU9yQUV5gKpgh_2owjtPczSYGTzf1ODWkFc
 */

class UserController extends Controller
{
    use HandleFavorite;

    /**
     * @OA\Post(
     *     path="/profile-update",
     *     operationId="update",
     *     tags={"Auth"},
     *     summary="редактирование",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="login",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="_method",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             default="put"
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function profileUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => ['required', 'min:8'],
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::find(Auth::guard('api')->id());
        $user->update([
            'email' => $request->get('email'),
            'login' => $request->get('login'),
            'password' => $request->get('password'),
        ]);
        $user->save();
        return response()->json(['message' => $user], Response::HTTP_OK);
    }


    /**
     * @OA\Get(
     *     path="/me",
     *     operationId="me",
     *     tags={"Auth"},
     *     summary="User",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     ),
     * )
     */
    public function me()
    {
        return response()->json(Auth::guard('api')->user(), Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/settings/allow-notifications",
     *     operationId="allow",
     *     tags={"Notification"},
     *     summary="User",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="allow_notifications",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="boolean",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     ),
     * )
     * @param $request
     * @return JsonResponse
     */
    public function allowNotifications(Request $request)
    {
        $user = User::find(Auth::guard('api')->id());
        $user->allow_notifications = filter_var($request->get('allow_notifications'), FILTER_VALIDATE_BOOLEAN);
        $user->save();
        return response()->json(['message' => 'Success'], Response::HTTP_OK);
    }


    /**
     * @OA\Post(
     *     path="/save-fcm-token",
     *     operationId="fcm_token",
     *     tags={"Notification"},
     *     summary="notify",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="fcm_token",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function saveFcmToken(Request $request)
    {
        $user = User::query()->find(\auth()->guard('api')->id());
        $user->fcm_token = $request->get('fcm_token');
        $user->save();

        return response()->json([
            'user' => $user,
            'message' => 'User token updated successfully',
        ]);

    }
}
