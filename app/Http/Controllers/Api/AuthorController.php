<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleSubscribe;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    use HandleSubscribe;
    /**
     * @OA\Get(
     *     path="/authors/{id}",
     *     operationId="id",
     *     tags={"Автор"},
     *     summary="id",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found",
     *         @OA\JsonContent()
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $user = User::query()->findOrFail($id);
        if($user->isAuthor()){
            $user->load( 'events', 'author', 'medias', 'userSubscribes');
            return response()->json(UserResource::make($user), Response::HTTP_OK);
        } else{
            return response()->json(['error' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }
    }


    /**
     * @OA\Post(
     *     path="/authors/{id}/subscribe",
     *     operationId="Users subscribed",
     *     tags={"Автор"},
     *     summary="subscribed",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="subscribed",
     *         in="query",
     *         description="boolean",
     *         required=false,
     *         @OA\Schema(
     *             type="boolean",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     * @param $id
     * @return JsonResponse
     */
    public function subscribe($id)
    {
        $user = User::query()->findOrFail($id);
        $result = $this->saveSubscribe($user->id, User::class, \request()->get('subscribed'), auth()->guard('api')->id());
        return response()->json(['type' => $result], Response::HTTP_OK);
    }
}
