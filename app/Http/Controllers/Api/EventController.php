<?php

namespace App\Http\Controllers\Api;

use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HandleFavorite;
use App\Http\Controllers\MediaHandler;
use App\Http\Resources\EventResource;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class EventController extends Controller
{
    use MediaHandler;
    use HandleFavorite;

    /**
     * @OA\Get(
     *     path="/events",
     *     operationId="page",
     *     tags={"События"},
     *     summary="Page",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="The page number",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     */
    public function index()
    {
//        $events = Event::query()->where('active', 1)->get();
//        foreach ($events as $event) {
//            if (!$event->period) {
//                continue;
//            }
//            $periodDate = (new DateTime($event->start_date_time))->modify('+' . $event->period . 'days');
//            $tomorrow = new DateTime("tomorrow");
//
//            if ($periodDate < $tomorrow) {
//                $event->active = 0;
//                $event->save();
//            }
//        }
        $events = Event::with('medias', 'places.medias', 'user.medias', 'places.userFavorites', 'places.userSubscribes', 'userFavorites')
            ->where('active', 1)
            ->where('start_date_time', '>=', Carbon::now())
            ->paginate();

        return response()->json(EventResource::collection($events)->response()->getData(true), Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *     path="/events/{id}",
     *     operationId="id",
     *     tags={"События"},
     *     summary="id",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="detail",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $event = Event::with('medias', 'places.medias', 'user.medias', 'places.userFavorites', 'places.userSubscribes', 'userFavorites')->findOrFail($id);
        return response()->json(EventResource::make($event), Response::HTTP_OK);
    }


    /**
     * @OA\Post(
     *     path="/events/{id}/favorite/{like}",
     *     operationId="Event favorite",
     *     tags={"События"},
     *     summary="like",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="like",
     *         in="path",
     *         description="",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     * @param $id
     * @param $like
     * @return JsonResponse
     */
    public function favorite($id, $like)
    {
        $event = Event::query()->findOrFail($id);
        $result = $this->saveFavorite($event->id, Event::class, $like, auth()->guard('api')->id());
        return response()->json(['type' => $result], Response::HTTP_OK);
    }


    /**
     * @OA\Get(
     *     path="/events/favorites",
     *     operationId="Events favorites",
     *     tags={"События"},
     *     summary="event likes",
     *     security={
     *         {"Bearer": {}},
     *     },
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="The page number.",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Example not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     */
    public function favorites()
    {
        $eventIds = $this->getFavoriteIds(auth()->guard('api')->id(), Event::class);
        $events = Event::with('userFavorites', 'medias', 'places.medias', 'user.medias', 'places.userFavorites', 'places.userSubscribes')
            ->whereIn('id', $eventIds)
            ->paginate();

        return response()->json(EventResource::collection($events)->response()->getData(true), Response::HTTP_OK);
    }

}
