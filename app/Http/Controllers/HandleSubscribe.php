<?php


namespace App\Http\Controllers;


use App\Favorite;
use App\Subscribe;

trait HandleSubscribe
{
    public function saveSubscribe($subscribableId, $subscribablesType, $subscribed, $userId)
    {
        $subscribe = Subscribe::query()
            ->where('user_id', $userId)
            ->where('subscribable_id', $subscribableId)
            ->where('subscribable_type', $subscribablesType)
            ->first();

        if (!$subscribe){
            $subscribe = Subscribe::query()->create([
                'user_id'           =>$userId,
                'subscribable_id'    => $subscribableId,
                'subscribable_type'  => $subscribablesType,
                'sub_scribed' => filter_var($subscribed, FILTER_VALIDATE_BOOLEAN)
            ]);
        } else {
            $subscribe->sub_scribed = filter_var($subscribed, FILTER_VALIDATE_BOOLEAN);
            $subscribe->save();
        }
        return ($subscribe->sub_scribed > 0) ? 'subscribe' : 'unsubscribe';
    }

    public function getSubscribeIds($userId, $subscribablesType)
    {
        return Subscribe::query()
            ->where('user_id', $userId)
            ->where('subscribable_type', $subscribablesType)
            ->pluck('subscribable_id');
    }
}
