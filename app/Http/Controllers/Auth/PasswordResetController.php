<?php


namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\PasswordResetCode;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PasswordResetController extends Controller
{

    /**
     * @OA\Post(
     *     path="/password/email",
     *     operationId="ForgotPassword",
     *     tags={"Auth"},
     *     summary="Забыли пароль",
     *     security={
     *         {"app_id": {}},
     *     },
     *      @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     * )
     * @param Request $request
     * @return Response
     * @throws Exception
     */

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return new Response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $email = $request->get('email');
        $user = User::where('email', $email)->orWhere('login', $email)->first();
        if (!$user) {
            return new Response([], Response::HTTP_NOT_FOUND);
        }

        $passwordReset = PasswordResetCode::query()->where('user_id', $user->id)->first();
        if ($passwordReset){
            $passwordReset->delete();
        }

        while (true){
            $code = rand(100000, 999999);
            $passwordReset = PasswordResetCode::query()->where('code', $code)->first();
            if (!$passwordReset){
                $passwordReset = PasswordResetCode::query()->create([
                   'user_id' => $user->id,
                   'code' => $code
                ]);
                break;
            }
        }

        if ($user->email){
            try {
                Mail::raw('Код для сброса пароля: '.$code, function ($message) use($user) {
                    $message->subject('Сброс пароля');
                    $message->from(env('MAIL_USERNAME', ''), env('APP_NAME'));
                    $message->to($user->email);
                });
            }catch (Exception $exception){
                info('Сброс пароля', [$exception->getMessage()]);
            }

        }


        return new Response(['code' => $passwordReset->code], Response::HTTP_CREATED);
    }

    /**
     * @OA\Post(
     *     path="/password/reset",
     *     operationId="reset",
     *     tags={"Auth"},
     *     summary="Сброс пароля",
     *     security={
     *         {"app_id": {}},
     *     },
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="code",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     * )
     * @param Request $request
     * @return JsonResponse|Response
     * @throws Exception
     */

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' =>  ['required', 'string'],
            'password' => ['required', 'string', 'min:8'],
            'code' => ['required'],
        ]);

        if ($validator->fails()) {
            return new Response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $email = $request->get('email');
        $user = User::where('email', $email)->orWhere('login', $email)->first();
        if (!$user) {
            return new Response(['message' => trans('passwords.user')], Response::HTTP_NOT_FOUND);
        }

        $passwordReset = PasswordResetCode::query()
            ->where('user_id', $user->id)
            ->where('code', $request->get('code'))
            ->first();

        if (!$passwordReset) {
            return new Response(['message' => trans('passwords.code')], Response::HTTP_NOT_FOUND);
        }

        $user->password = $request->get('password');
        $user->save();
        $passwordReset->delete();
        return new Response(['message' => 'success'], Response::HTTP_OK);
    }
}
