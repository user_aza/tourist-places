<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\PasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;


    public function sendResetLinkEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);


        $passwordReset = PasswordReset::query()->where('email', $user->email)->first();

        if (!$passwordReset){
            $passwordReset = PasswordReset::query()->create([
                    'email' => $user->email,
                    'token' => Str::random(60)
                ]);
        }

        $url = route('password.token', ['token' => $passwordReset->token]);
        return response()->json([
            'url' => $url
        ]);
//        if ($user && $passwordReset)
//            $user->notify(
//                new PasswordResetRequest($passwordReset->token)
//            );
        return response()->json([
            'message' => 'We have e-mailed your password reset link!',
            'url' => $passwordReset->toArray()
        ]);


//        $validator = Validator::make($request->all(), ['email' => 'required|email']);
//
//        if ($validator->fails()) {
//            return new Response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
//        }
//        $response = $this->broker()->sendResetLink($this->credentials($request));
//
//        if ($response == Password::RESET_LINK_SENT){
//           return new JsonResponse(['message' => trans($response)], 200);
//        } else {
//            return new JsonResponse(['message' => trans($response)], 200);
//        }

    }




}
