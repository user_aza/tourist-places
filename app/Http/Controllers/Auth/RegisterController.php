<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @OA\Post(
     *     path="/register",
     *     tags={"Auth"},
     *     operationId="register",
     *     summary="Регистрация пользователей",
     *     security={
     *         {"app_id": {}},
     *     },
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="login",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     * )
     *
     * Store a newly created resource in storagee.
     *
     * @param Request $request
     * @return Response
     */

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return new Response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        $user->generateToken();
        return new Response(['user' => $user], Response::HTTP_OK);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['email', 'max:255', 'unique:users'],
            'login' => ['required', 'unique:users', 'max:20'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => isset($data['email']) ? $data['email'] : null,
            'login' => isset($data['login']) ? $data['login'] : null,
            'password' => $data['password'],
            'active' => 1,
            'role_id' => User::ROLE_AUTHOR_ID,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/check-login",
     *     operationId="checkLogin",
     *     tags={"Auth"},
     *     summary="login",
     *     security={
     *         {"app_id": {}},
     *     },
     *     @OA\Parameter(
     *         name="login",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     * )
     * @param Request $request
     * @return Response
     */

    public function checkLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required|unique:users|max:20',
        ]);

        if ($validator->fails()) {
            return new Response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } else{
            return new Response(['message' => 'success'], Response::HTTP_OK);
        }
    }


    /**
     * @OA\Post(
     *     path="/check-email",
     *     operationId="checkEmail",
     *     tags={"Auth"},
     *     summary="email",
     *     security={
     *         {"app_id": {}},
     *     },
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine"
     *     ),
     * )
     * @param Request $request
     * @return Response
     */

    public function checkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        if ($validator->fails()) {
            return new Response(['errors' => $validator->errors()->all()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } else{
            return new Response(['message' => 'success'], Response::HTTP_OK);
        }
    }
}

