<?php


namespace App\Http\Controllers;


use App\Media;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

trait MediaHandler
{
    public function createMedia($files, $model, $main = 0, $path = 'medias')
    {
        if ($files){
            if (is_array($files)) {
                foreach ($files as $file) {
                    $this->createMediaWithRelation($path, $file, $model, $main);
                }
            } else {
                $this->createMediaWithRelation($path, $files, $model, $main);
            }
        }
    }



    public function storeMedia($files)
    {
        foreach ($files as $file) {
            Media::query()->create([
                'src' => Storage::disk('public')->put('medias', $file),
            ]);
        }
    }

    public function deleteImageWithDetach($id)
    {
        $media = Media::query()->find($id);
        DB::table('mediables')->where([
            'media_id' => $media->id,
        ])->delete();
        Storage::disk('public')->delete('medias/'.$media->src);
        $media->delete();
    }


    public function createMediaWithRelation(string $path, $file, $model, $main): void
    {
        $media = Media::query()->create([
            'src' => Storage::disk('public')->put($path, $file),
            'main' => $main
        ]);

        DB::table('mediables')->insert([
            'media_id' => $media->id,
            'mediables_id' => $model->id,
            'mediables_type' => get_class($model)
        ]);
    }
}
