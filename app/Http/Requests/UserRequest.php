<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->segment(2) == 'users' && $this->segment(4) == 'update'){
            return [
                'name' => 'required|min:3',
                'login' => 'required|max:20|unique:users,login,'.$this->segment(3),
                'email' => 'required|email|unique:users,email,'.$this->segment(3),
                'role_id' => 'required',
            ];
        }

        return [
            'name' => 'required|min:3',
            'login' => 'required|unique:users,login|max:20/',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'role_id' => 'required',
        ];
    }
}
