<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        $guard = $guards[0];
        if (Auth::guard($guard)->guest()) {
                return ($guard == 'api') ?
                    response('Unauthorized User', Response::HTTP_UNAUTHORIZED) :
                    redirect()->route('admin.auth.form');
        }
        return $next($request);
    }
}
