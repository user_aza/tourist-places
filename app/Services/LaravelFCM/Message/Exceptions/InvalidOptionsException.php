<?php
namespace App\Services\LaravelFCM\Message\Exceptions;


use Exception;

/**
 * Class InvalidOptionsException.
 */
class InvalidOptionsException extends Exception
{
}
