<?php

namespace App\Services\LaravelFCM\Message\Exceptions;

use Exception;

/**
 * Class NoTopicProvidedException.
 */
class NoTopicProvidedException extends Exception
{
}
