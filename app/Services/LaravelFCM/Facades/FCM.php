<?php

namespace App\Services\LaravelFCM\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static sendTo(array $tokens, \App\Services\LaravelFCM\Message\Options $option, \App\Services\LaravelFCM\Message\PayloadNotification $notification, \App\Services\LaravelFCM\Message\PayloadData $data)
 */
class FCM extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fcm.sender';
    }
}
