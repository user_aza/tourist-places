<?php


namespace App\Services\Filters;


use Illuminate\Database\Eloquent\Builder;

class Equal extends FilterForm
{
    public $attribute;
    public $label;
    public $options;

    public function __construct($options = [])
    {
        $this->options = $options;
    }

    public function setField(string $attribute, $label)
    {
        $this->attribute = $attribute;
        $this->label = $label;
        return $this;
    }

    public function query(Builder $query, $requestAll)
    {
        $value = $this->getValue($this->attribute, $requestAll);
        if ($value or is_numeric($value)) {
            $query->where($this->attribute, '=', $value);
        }
    }

    function getHtmlElement()
    {
        if ($this->options){
            return view('admin.elements.filter.select', ['data' => $this])->render();
        } else {
            return view('admin.elements.filter.input', ['data' => $this])->render();
        }
    }
}
