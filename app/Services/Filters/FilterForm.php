<?php


namespace App\Services\Filters;


abstract class FilterForm
{
    abstract function setField(string $attribute, $label);

    abstract function getHtmlElement();

    abstract function query(\Illuminate\Database\Eloquent\Builder $query, $requestAll);

    public function getValue($key, $array)
    {
        return isset($array[$key]) ? $array[$key] : null;
    }
}
