<?php


namespace App\Services\Filters;


use Illuminate\Database\Eloquent\Builder;

class Like extends FilterForm
{
    public $attribute;
    public $label;

    public function setField(string $attribute, $label)
    {
        $this->attribute = $attribute;
        $this->label = $label;
    }

    public function query(Builder $query, $requestAll)
    {
        if ($value = $this->getValue($this->attribute, $requestAll)) {
            $query->where($this->attribute, 'like', "%$value%");
        }
    }

    function getHtmlElement()
    {
        return view('admin.elements.filter.input', ['data' => $this])->render();
    }
}
