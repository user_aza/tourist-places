<?php


namespace App\Services\Filters;


use Illuminate\Database\Eloquent\Builder;

class Filter
{
    private $elements = '';
    /**
     * @var Builder
     */
    private $query;
    private $requestAll;

    public function __construct(Builder $query, $requestAll)
    {
        $this->query = $query;
        $this->requestAll = $requestAll;
    }

    public function addField(string $attribute, $label, FilterForm $filter)
    {
        $filter->setField($attribute, $label);
        $this->elements .= $filter->getHtmlElement();
        $filter->query($this->query, $this->requestAll);
    }

    public function getForm(string $submitRoute, string $resetRoute)
    {
        return view('admin.elements.filter.form', [
            'formElements' => $this->elements,
            'submitRoute' => $submitRoute,
            'resetRoute' => $resetRoute
        ])->render();
    }
}
