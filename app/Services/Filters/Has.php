<?php


namespace App\Services\Filters;


use Illuminate\Database\Eloquent\Builder;

class Has extends FilterForm
{
    public $relation;
    public $options;
    public $attribute;
    public $label;

    public function __construct($relation, $options)
    {
        $this->relation = $relation;
        $this->options = $options;
    }

    public function setField(string $attribute, $label)
    {
        $this->attribute = $attribute;
        $this->label = $label;
        return $this;
    }

    public function query(Builder $query, $requestAll)
    {
        if ($value = $this->getValue($this->attribute, $requestAll)) {
            if (is_numeric($value)) {
                $attribute = $this->attribute;
                $query->whereHas($this->relation, function ($q) use ($value, $attribute) {
                    $q->where($attribute, $value);
                });
            }
        }
    }


    function getHtmlElement()
    {
        return view('admin.elements.filter.select', ['data' => $this])->render();
    }
}
