<?php


namespace App\Services\Filters;


use Illuminate\Database\Eloquent\Builder;

class Between extends FilterForm
{
    public $attribute;

    public $label;
    private $min = 1;
    private $max;


    public function __construct($max)
    {
        $this->max = $max;
    }

    public function setField(string $attribute, $label)
    {
        $this->attribute = $attribute;
        $this->label = $label;
    }

    public function query(Builder $query, $requestAll)
    {
        if ($between = $this->getValue($this->attribute, $requestAll)) {
            if (!$between[0]) $between[0] = $this->min;
            if (!$between[1]) $between[1] = $this->max;
            $query->whereBetween($this->attribute, $between);
        }
    }

    function getHtmlElement()
    {
        return view('admin.elements.filter.between_input', ['data' => $this])->render();
    }
}
