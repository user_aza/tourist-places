<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $all)
 * @method static find($id)
 * @method static get()
 * @method static pluck(string $string, string $string1)
 */
class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'id',
        'name',
        'description'
    ];

    public function user(){
        return $this->hasMany(User::class, 'role_id');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }
}
