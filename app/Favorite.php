<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favoritables';
    protected $fillable = [
        'id',
        'user_id',
        'favoritable_id',
        'favoritable_type',
        'li_ke'
    ];

    public function favoritable()
    {
        return $this->morphTo();
    }
}
