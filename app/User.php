<?php

namespace App;

use App\Notifications\PasswordResetNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @method static create(array $all)
 * @method static find($id)
 * @method static where(string $string, $token)
 * @method static get()
 * @property mixed|string api_token
 */
class User extends Authenticatable
{
    use Notifiable;
    const ROLE_ADMIN_ID = 1;
    const ROLE_AUTHOR_ID = 2;

    protected $table = 'users';
    protected $fillable = [
        'id',
        'name',
        'login',
        'email',
        'password',
        'active',
        'role_id',
        'allow_notifications',
        'api_token',
        'fcm_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->role_id === self::ROLE_ADMIN_ID;
    }

    public function isAuthor()
    {
        return $this->role_id === self::ROLE_AUTHOR_ID;
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function author()
    {
        return $this->hasOne(Author::class);
    }

    public function medias()
    {
        return $this->morphToMany(Media::class, 'mediables');
    }


    public function events()
    {
        return $this->hasMany(Event::class, 'user_id');
    }

    public function favoriteEvents()
    {
        return $this->morphTo();
    }

    public function subscribes()
    {
        return $this->morphMany(Subscribe::class, 'subscribable');
    }

    public function userSubscribes()
    {
        return $this->morphMany(Subscribe::class, 'subscribable')->where('user_id', auth()->guard('api')->id());
    }


    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    public function generateToken()
    {
        $this->api_token = Str::random(60);
        $this->save();

        return $this->api_token;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }
    public function getNotyTitle($subscribable)
    {
        return $subscribable->name;
    }

    public function getNotyBody($subscribable)
    {
        return 'У нас намечается что-то интересное!';
    }
}
