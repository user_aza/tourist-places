<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordResetCode extends Model
{
    protected $table = "password_reset_codes";

    protected $fillable = [
        'id',
        'user_id',
        'code'
    ];
}
