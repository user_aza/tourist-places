<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static get()
 * @method static create(array $all)
 * @method static find($id)
 */
class Tag extends Model
{
    protected $table = 'tags';
    protected $dates = [
        'updated_at',
        'created_at',
    ];
    protected $fillable = [
        'id',
        'name',
        'description',
    ];

    public function places()
    {
        return $this->morphedByMany(Place::class, 'taggable');
    }

    public function events()
    {
        return $this->morphedByMany(Event::class, 'taggable');
    }

    public function userSubscribes()
    {
        return $this->morphMany(Subscribe::class, 'subscribable')->where('user_id', auth()->guard('api')->id());
    }

    public function subscribes()
    {
        return $this->morphMany(Subscribe::class, 'subscribable');
    }

    public function getNotyTitle($subscribable)
    {
        return 'Поездки на ' . $subscribable->name;
    }
    public function getNotyBody($subscribable)
    {
        return 'Вы давно этого хотели! Готовы посмотреть что вас ждёт?';
    }
}
