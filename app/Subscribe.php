<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    protected $table = 'subscribables';
    protected $fillable = [
        'id',
        'user_id',
        'subscribable_id',
        'subscribable_type',
        'sub_scribed'
    ];

    public function subscribable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
