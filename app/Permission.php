<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static get()
 * @method static create(array $all)
 * @method static find($id)
 * @method insertGetId(string[] $permission)
 */
class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillable = [
        'id',
        'name'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_permissions');
    }
}
