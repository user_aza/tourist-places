<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $all)
 * @method static find($id)
 * @method static get()
 */
class Event extends Model
{
    protected $table = 'events';
    protected $dates = [
        'updated_at',
        'created_at',
    ];
    protected $fillable = [
        'id',
        'name',
        'price',
        'description',
        'period',
        'start_date_time',
        'start_place',
        'user_id',
        'active',
    ];

    public function getStartDateTimeAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function places()
    {
        return $this->belongsToMany(Place::class, 'event_places');
    }

    public function medias()
    {
        return $this->morphToMany(Media::class, 'mediables');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favoritable');
    }

    public function userFavorites()
    {
        return $this->morphMany(Favorite::class, 'favoritable')->where('user_id', auth()->guard('api')->id());
    }
}
