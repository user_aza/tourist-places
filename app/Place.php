<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $all)
 * @method static find($id)
 * @method static get()
 */
class Place extends Model
{
    protected $table = 'places';
    protected $dates = [
        'updated_at',
        'created_at',
    ];
    protected $fillable = [
        'id',
        'name',
        'description',
        'lat',
        'lng',
    ];

    public function medias()
    {
        return $this->morphToMany(Media::class, 'mediables');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_places');
    }

//    public function nearestEvents()
//    {
//        return $this->belongsToMany(Event::class, 'event_places')
//            ->whereDate('start_date_time','>', now())
//            ->orderBy('start_date_time', 'asc')
//            ->limit(10);
//    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favoritable');
    }

    public function userFavorites()
    {
        return $this->morphMany(Favorite::class, 'favoritable')->where('user_id', auth()->guard('api')->id());
    }

    public function userSubscribes()
    {
        return $this->morphMany(Subscribe::class, 'subscribable')->where('user_id', auth()->guard('api')->id());
    }

    public function subscribes()
    {
        return $this->morphMany(Subscribe::class, 'subscribable');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function getNotyTitle($subscribable)
    {
        return 'УХУ, новое событие! 🤗';
    }
    public function getNotyBody($subscribable)
    {
        return $subscribable->name.' больше без вас не может. Заходите!';
    }
}
